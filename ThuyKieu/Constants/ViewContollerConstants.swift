//
//  ViewContollerConstants.swift
//  Helio
//
//  Created by Rich Nguyen on 7/20/16.
//  Copyright © 2016 Helio LLC. All rights reserved.
//

import UIKit

// MARK: STORY BOARD
let REGISTER_STORYBOARD                                 = "Register"
let RECENTLY_STORYBOARD                                 = "Recently"
let MENU_PAGE_STORYBOARD                                 = "MenuPageView"
let LEFT_MENU_STORYBOARD                                 = "LeftMenu"



// MARK: VIEW CONTROLLERS
let WELCOME_VIEW_CONTROLER                             = "WelcomeViewController"
let SIGN_IN_VIEW_CONTROLER                             = "SignInViewController"
let SIGN_UP_VIEW_CONTROLER                             = "SignUpAccountViewController"

let RECENTLY_ONE_VIEW_CONTROLER                             = "RecentlyOneViewController"
let RECENTLY_TWO_VIEW_CONTROLER                             = "RecentlyTwoViewController"
let RECENTLY_THREE_VIEW_CONTROLER                           = "RecentlyThreeViewController"
let SUB_CHAP_THUYKIEU_VIEW_CONTROLER                           = "SubChapThuyKieuViewController"
let READ_NEARLY_VIEW_CONTROLER                           = "ReadNearlyViewController"
let INVITE_FRIENDS_VIEW_CONTROLER                           = "InviteFriendsViewController"
let SETTING_VIEW_CONTROLER                           = "SettingViewController"
let CONTACTS_VIEW_CONTROLER                             = "ContactUsViewController"
let THUYKIEU_VIEW_CONTROLER                           = "ThuyKieuViewController"
let SUCKHOE_GIOITINH_VIEW_CONTROLER                           = "SucKhoeGioiTinhViewController"
let TIN_NONG_VIEW_CONTROLER                           = "TinNongViewController"
let TAMSU_VIEW_CONTROLER                           = "TamSuViewController"
let VIDEO_VIEW_CONTROLER                           = "VideoViewController"

let FORGOT_PASSWORD_VIEW_CONTROLER                     = "ForgotPasswordViewController"
let SEND_PASSWORD_VIEW_CONTROLER                       = "SendPasswordViewController"







// MARK: NIB NAME
let REPRECENT_SUB_CHAP_CELL_NIB                         = "RepresentSubChapCell"
let CHAP_CELL_NIB                                       = "ChapCell"
let CHAP_MODULE_CELL_NIB                                 = "ChapModuleCell"
let READ_NEARLY_XIB                                 = "ReadNearlyViewController"
let INVITE_FRIENDS_XIB                                 = "InviteFriendsViewController"
let MAIN_MENU_XIB                                 = "MainMenuCell"
let TEXT_FIELD_CELL                                 = "TextFieldCell"
let BUTTON_CELL                                 = "ButtonCustomCell"
let TITLE_LABEL_CELL                                 = "TittleLabelCell"
let SINGLE_LABEL_CELL                                 = "SingleLabelCell"





// MARK: Identifier Cell
let REPRECENT_IDENTIFIER_CELL                           = "representCell"
let CHAP_IDENTIFIER_CELL                                = "ChapCell"
let CHAP_MODULE_IDENTIFIER_CELL                         = "chapCell"
let MAIN_MENU_CELL                                      = "MainMenuCell"
let SINGLE_LABEL_IDENTIFIER_CELL                        = "singleCell"






let CREATE_ACCOUNT_VIEW_CONTROLER                      = "CreateAccountViewController"



// MARK: MAIN WORKFLOW
let MAIN_MENU_VIEW_CONTROLLER                           = "MainMenuViewController"
let HOME_VIEW_CONTROLLER                                = "HomeViewController"
let MEMBER_SHIP_VIEW_CONTROLLER                         = "MemberShipViewController"

