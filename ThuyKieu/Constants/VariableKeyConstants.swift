//
//  Constants.swift
//  Helio
//
//  Created by Rich Nguyen on 7/20/16.
//  Copyright © 2016 Helio LLC. All rights reserved.
//

import UIKit

struct LANGUAGE_APP {
    static let VIETNAMESE = "vn"
    static let ENGLISH = "en"

}

struct MoneyLevel {
    static let LEVEL_ONE = "1.99"
}


let DATE_FORMAT_DISPLAY = "dd.MM.YY"
let NUMBER_INIT_DOUBLE : Double  = -999999999999
let KEY_FOLDER_IMAGE_FROM_SEVER = "ImageAccounts"

struct Price_Level {
    static let LEVEL_ONE = "Khách Vãng Lai    $1.99/mo"
    static let LEVEL_SECOND = "Khách thường     $3.99/mo"
    static let LEVEL_THIRD = "Thượng Khách    $5.99/mo"
    static let BUTTON_NAME = "Submit"
}


struct DataConstant {
    static let insecure  = "cool"
    static let appName = "ThuyKieu"
}

struct NotificationKey {
    static let DISMISS_POPOVER                                          = "DismissPopover"
    static let DISMISS_KEYBOARD                                         = "DismissKeyboard"
}

struct USER_MENU_IMAGE {
    static let MY_ACCOUNT       = ""
    static let MY_HELIO_CARD    = ""
    static let SEND_FEEDBACK    = ""
    static let NOTIFICATION     = ""
    static let SIGN_IN          = ""
    static let CREAT_ACCOUNT    = ""
}

struct USER_MENU {
    static let MY_ACCOUNT       = "My Account"
    static let MY_HELIO_CARD    = "My Helio Card"
    static let SEND_FEEDBACK    = "Send Feedback"
    static let NOTIFICATION     = "Notification"
    static let SIGN_IN          = "Login"
    static let CREAT_ACCOUNT    = "Create Account"
    static let LOGOUT           = "Đăng xuất"
}

struct GeneralConfig {
    static let NUMBER_INVALID_PIN : Int              = 3
    static let TIME_BLOCK : Double                   = 1
    static let SECOND  : Double                      = 60
    static let CHECK_TIME_SECOND : Double            = 30
    static let MINUTES_TIMEOUT: Double               = 300.0 //60 * 5
    static let MONTH_LIMIT : Int                     = 6
    static let MINUS_WEEK                            = -7
}

struct KeyFeedBack {
    static let NUMBER_CHARACTER_FEEDBACK             = 257
    static let urlAppStore                           = "https://itunes.apple.com/us/app/Helio/id1181860241?ls=1&mt=8"
}

struct KeyFAQs {
    static let    FAQ                            = "FAQ"
    static let    HTML                           = "html"
}

struct KeyFormatDate {
    static let DAYMONTH                          = "dd.M"
}

struct KeyCallSupport {
    static let PHONE_CALL_SUPPORT       = "+41 44 659 64 92"
}

struct KeyUserDefault {
    static let IS_LOGIN                  = "isLogin"
    
}

struct ErrorMessageValidate {
    //Username
    static let usernameRequired             = "PLEASE_ENTER_YOUR_USERNAME".localized()

}

struct ValidatorConfig {
    static let userNameMinLength = 2

}

struct DeviceType {
    static let IS_IPHONE_4       = "iPhone 4"
    static let IS_IPHONE_4S      = "iPhone 4s"
    static let IS_IPHONE_5       = "iPhone 5"
    static let IS_IPHONE_5C      = "iPhone 5c"
    static let IS_IPHONE_5S      = "iPhone 5s"
    static let IS_IPHONE_6       = "iPhone 6"
    static let IS_IPHONE_6_PLUS  = "iPhone 6 Plus"
    static let IS_IPHONE_6S      = "iPhone 6s"
    static let IS_IPHONE_6S_PLUS = "iPhone 6s Plus"
}

struct AppFonts {
    static let mainFontRegular	= "SFUIText-Regular"
    static let mainFontBold		= "Oxygen-Bold"
    static let mainFontLight	= "SFUIText-Light"
    static let mainNameFont     = "SFUIText"
    
    static let cardFont			= "AvenirHeavy"
    static let cardFontBold		= "AvenirHeavy-Bold"
    static let cardFontLight	= "AvenirHeavy-Light"
    static let oxygenRegular	= "Oxygen-Regular"

}

enum TypeUser {
    case GuestUser
    case ThuyKieuUser
    case StaffUser
}

typealias JSONDictionary = [String: AnyObject]
typealias JSONArray      = Array<AnyObject>










