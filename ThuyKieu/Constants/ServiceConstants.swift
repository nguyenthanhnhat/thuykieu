//
//  ServiceConstants.swift
//  Helio
//
//  Created by Rich Nguyen on 8/25/16.
//  Copyright © 2016 Helio LLC. All rights reserved.
//

import Foundation

struct KeyService {
    static let RequestSuccessValue      = "0"
    static let success                  = "success"
    static let responseMessage          = "responseMessage"
    static let walletID                 = "walletID"
    static let Accept                   = "Accept"
    static let Accept_Type              = "application/json"
    static let ContentType              = "Content-Type"
    static let domain                   = "my.client.error"
    static let applicationID            = "applicationID"
    static let sessionID                = "sessionID"
    static let productVersion           = "productVersion"
    static let applicationVersion       = "applicationVersion"
    static let timestamp                = "timestamp"
    static let timezone                 = "timezone"
    static let locale                   = "locale"
    static let location                 = "location"
    static let messageID                = "messageID"
    static let deviceTokenID            = "deviceTokenID"
    static let applicationName          = "applicationName"
    static let Authorization            = "Authorization"
    static let deviceFingerprint        = "deviceFingerprint"
    static let applicationIdValue       = "13b5b092-a57a-48c8-8881-95a042d38e4b"
    static let sessionIDValue           = "31447c45-e5f6-468b-a1bf-605d1933e8e0"
    static let productVersionValue      = "1.0"
    static let applicationVersionValue  = "6.0.1"
    static let timestampValue           = "2010-03-08 14:59:30.252"
    static let timezoneValue            = "GMT-08:00"
    static let localeValue              = "en_US"
    static let messageIDValue           = "1234567890abcdef"
    static let deviceTokenIDValue       = "1234567890"
    static let applicationNameValue     = "HelioMobile"
    static let AuthorizationValue       = "bear 1234567890"
    static let longitude                = "longitude"
    static let latitude                 = "latitude"
    static let altitude                 = "altitude"
    static let authToken                = "authToken"
    static let applicationInstanceID    = "applicationInstanceID"
    static let applicationInstanceIDValue = "31447c45-e5f6-468b-a1bf-605d1933e8e0"
    static let accessToken              = "accessToken"
    static let accessTokenValue         = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"
    static let userAgent                = "User-Agent"
    
}

struct StatusTerm {
    static var newVersionMtp               = "1.0"
    static var newVersionIam               = "1.0"
}

struct CategoryValue {
    static var ThuyKieu = ""
    static var SkhoeGtinh = "19"
    static var TamSu = "24"
    static var TinNong = "20"
    static var Video = "25"
}




struct KeyHttpMethod {
    static let POST                     = "POST"
    static let GET                      = "GET"
    static let PUT                      = "PUT"
}

//MARK: - Defide error Client
struct ErrorClient {
    static let NO_INTERNET_CONNECTION   = 111111
    static let REQUEST_TIME_OUT         = 111112
    static let SERVER_IS_NOT_FOUND      = 111113
    static let INTERNAL_ERROR           = 111114
    static let SYS_TEM_ERROR            = 111115
}
struct MessageErrorClient {
    static let NO_INTERNET_CONNECTION_111111    = "ERR_INTERNET".localized()
    static let REQUEST_TIME_OUT_111112          = "ERR_REQUEST".localized()
    static let SERVER_IS_NOT_FOUND_111113       = "ERR_REQUEST".localized()
    static let INTERNAL_ERROR_111114            = "ERROR_UNDEFINED".localized()
    static let SYS_TEM_ERROR_111115             = "ERROR_GENERAL".localized()
}

func getServiceUrl()-> String {
    return getDevEnviroment() ? "http://thuykieu.com/api/" : "http://thuykieu.com/api/"


}
//MARK: - UrlMethod
struct UrlMethod {

    //MARK: - REGISTER
    static let THUYKIEU_SERVICE                     = getServiceUrl()
    static let LOGIN_SERVICE                        = getServiceUrl() + "user/generate_auth_cookie/"
    static let REGISTER_SERVICE                     = getServiceUrl() + "user/register/"
    static let CHECK_EXISTING_EMAIL                 = getServiceUrl() + "user/find_email/"


    static let GET_LEVELS                           = getServiceUrl() + "user/get_levels/?insecure=cool"

    //MARK: - CATEGORIES
    static let GET_CATEGORIES                       = getServiceUrl() + "get_categories/?parent=0"
    static let GET_SUB_CATEGORIES                   = getServiceUrl() + "get_categories/?parent=1"
    static let GET_SUB_CHANEL_BY_ID                 = getServiceUrl() + "get_category_posts/?id="
    static let GET_POST_DETAIL                 = getServiceUrl() + "get_post/?id="

}

