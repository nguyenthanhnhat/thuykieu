//
//  Component.swift
//  ThuyKieu
//
//  Created by Dragon Member on 7/20/16.
//  Copyright © 2016 ThuyKieu LLC. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


func checkExistFile(path: String)-> Bool{
    var isDir: ObjCBool = false
    let fm = FileManager.default
    if fm.fileExists(atPath: path, isDirectory: &isDir) {
        return true
    }
    return false
}


func createLogFile(){
    let file = "log.txt"
    let writingText = "=====LOG====="
    if let dir : NSString = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.libraryDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first as NSString? {
        let path = dir.appendingPathComponent(file);
        print("\(path)")

        if(!checkExistFile(path: path)){
            //writing
            do {
                try writingText.write(toFile: path, atomically: false, encoding: String.Encoding.utf8)
            } catch {
                /* error handling here */
            }
        
        }
    }
}
func writeLog(_ log: String){
    if let dir : NSString = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.libraryDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first as NSString? {
        let file = "log.txt"
        let path = dir.appendingPathComponent(file);
        //reading
        do {
            var readingText = try NSString(contentsOfFile: path, encoding: String.Encoding.utf8.rawValue)
            readingText = (readingText as String) + "\n" + log as NSString
            try readingText.write(toFile: path, atomically: false, encoding: String.Encoding.utf8.rawValue)
        }
        catch {
            /* error handling here */
        }
    }
    
}





