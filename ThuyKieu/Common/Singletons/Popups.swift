//
//  Popups.swift
//
//  Created by Tom Swindell on 25/05/2015.
//  Copyright (c) 2015 Tom Swindell. All rights reserved.
//

import Foundation
import UIKit
extension UIAlertController {
    func changeFont(_ view:UIView,font:UIFont) {
        for item in view.subviews {
            if item.isKind(of: UICollectionView.self) {
                let col = item as! UICollectionView
                for  row in col.subviews{
                    changeFont(row, font: font)
                }
            }
            if item.isKind(of: UILabel.self) {
                let label = item as! UILabel
                var fontName: String = AppFonts.oxygenRegular
                var fontSize: CGFloat = 16.0
                if self.title == label.text{
                   fontSize = 17.0
                   fontName = AppFonts.oxygenRegular
                } else if self.message == label.text {
                    fontSize = 13.0
                    fontName = AppFonts.oxygenRegular
                }
                let lblFont: UIFont = UIFont(name: fontName, size: fontSize)!
                label.font = lblFont
            }else {
                changeFont(item, font: font)
            }
            
        }
    }
    open override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let font = UIFont(name: AppFonts.oxygenRegular, size: 13.0)
        changeFont(self.view, font: font! )
    }
}
class Popups: NSObject {
    private static var __once: () = {
            Static.instance = Popups()
        }()
    struct Static {
        static var onceToken: Int = 0
        static var instance: Popups? = nil
    }
    class var SharedInstance: Popups {
        _ = Popups.__once
        return Static.instance!
    }
    var alertComletion : ((String) -> Void)!
    var alertButtons : [String]!
    var alertController : UIAlertController?
    
    func ShowAlert(_ sender: UIViewController, title: String, message: String, buttons : [String], completion: ((_ buttonPressed: String) -> Void)?) {
        
        let aboveIOS7 = floor(NSFoundationVersionNumber) > floor(NSFoundationVersionNumber_iOS_7_1)
        if(aboveIOS7) {
            
            alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            for b in buttons {
                alertController!.addAction(UIAlertAction(title: b, style: UIAlertActionStyle.default, handler: {
                    (action : UIAlertAction) -> Void in
                    completion!(action.title!)
                }))
            }
            sender.present(alertController!, animated: true, completion: nil)
            
        } else {
            
            self.alertComletion = completion
            self.alertButtons = buttons
            let alertView  = UIAlertView()
            alertView.delegate = self
            alertView.title = title
            alertView.message = message
            for b in buttons {
                
                alertView.addButton(withTitle: b)
                
            }
            alertView.show()
        }
        
    }
    
    func ShowPopup(_ title : String, message : String) {
        let alert: UIAlertView = UIAlertView()
        alert.title = title
        alert.message = message
        alert.addButton(withTitle: "Ok")
        alert.show()
    }
}

extension Popups: UIAlertViewDelegate {
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if(self.alertComletion != nil) {
            self.alertComletion!(self.alertButtons[buttonIndex])
        }
    }
    
}
