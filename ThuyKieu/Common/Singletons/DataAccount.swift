//
//  DataAccount.swift
//  ThuyKieu
//
//  Created by Dragon Member on 8/2/16.
//  Copyright © 2016 ThuyKieu LLC. All rights reserved.
//

import UIKit

//singleton class
class DataAccount {
    fileprivate static var instance : DataAccount!
    var keyRandom               : String? = ""
    var typeUser                : TypeUser = TypeUser.GuestUser
    var categories              : [CategoryObject]?
    var categoriesThuyKieu   : [CategoryObject]?
    var categoriesSubThuyKieu   : [CategoryObject]?
    var categoriesSKhoeGioiTinh : [CategoryObject]?
    var categoriesTamSu : [CategoryObject]?
    var categoriesTinNong : [CategoryObject]?
    var categoriesVideo : [CategoryObject]?
    var levels                  : [LevelObject]?
    var categoriesById : [CategoryObject]?
    var postDetails : PostDetails?
    var readNearlyChap : [CategoryObject]?
    var userInfo  : UserInfoObject?

    var thuyKieuChapListDetails : [PostDetails]?
    var skgtinhListDetails : [PostDetails]?
    var tinNongChapListDetails : [PostDetails]?
    var tSuChapListDetails : [PostDetails]?
    var videoChapListDetails : [PostDetails]?


    class func shared() -> DataAccount {
        self.instance = (self.instance ?? DataAccount())
        return self.instance
    }

    init() {
        keyRandom      = ""
        categories     = []
        categoriesSKhoeGioiTinh = []
        levels         = []
        categoriesTamSu = []
        categoriesThuyKieu = []
        categoriesSubThuyKieu = []
        categoriesVideo = []
        categoriesTinNong = []
        categoriesById = []
        postDetails = PostDetails()
        readNearlyChap = []
        thuyKieuChapListDetails = []
        skgtinhListDetails  = []
        tinNongChapListDetails  = []
        tSuChapListDetails  = []
        videoChapListDetails  = []
    }

}
