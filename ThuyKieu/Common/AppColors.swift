//
//  AppColors.swift
//  ThuyKieu
//
//  Created by Dragon Member on 7/25/16.
//  Copyright © 2016 ThuyKieu LLC. All rights reserved.
//

import UIKit

struct AppColors {



    static let backgroundLogoLeftMenu   = UIColor(red: 55/255, green: 33/255, blue: 58/255, alpha: 1.0)
    static let backgroundLeftMenu       = UIColor(red: 58/255, green: 42/255, blue: 55/255, alpha: 1.0)


    static let backgroundPageBar =  UIColor(red: 218/255, green: 223/255, blue: 229/255, alpha: 1.0)
//    static let backgroundRegisterColor =  UIColor(red: 243/255, green: 110/255, blue: 101/255, alpha: 1.0)
    static let backgroundRegisterColor = UIColor.white
    static let backgroundRegisterBlurredColor =  UIColor(red: 21/255, green: 6/255, blue: 43/255, alpha: 0.5)
    static let backgroundMainColor =  UIColor(red: 34/255, green: 19/255, blue: 56/255, alpha: 1.0)
    static let backgroundHomeColor =  UIColor(red: 66/255, green: 47/255, blue: 94/255, alpha: 1.0)
    static let backgroundWellComeColor =  UIColor(red: 221/255, green: 216/255, blue: 228/255, alpha: 1.0)

    static let yellowDeselectedColor =  UIColor(red: 254/255, green: 194/255, blue: 49/255, alpha: 0.6)
    static let yellowSelectedColor =  UIColor(red: 254/255, green: 194/255, blue: 49/255, alpha: 1.0)
    static let yellowColor =  UIColor(red: 254/255, green: 194/255, blue: 49/255, alpha: 1.0)


    static let boderButtonColor = UIColor(red: 255, green:255, blue: 255, alpha: 0.5)
    static let titleColor = UIColor(red: 255, green:255, blue: 255, alpha: 1.0)
    static let textfiledColor = UIColor(red: 255, green:255, blue: 255, alpha: 1.0)
    static let lineTextField = UIColor(red: 255, green:255, blue: 255, alpha: 0.9)
    static let linkTextColor = UIColor(red: 218/255, green:19/255, blue: 154/255, alpha: 1.0)}
