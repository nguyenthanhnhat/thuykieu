
import UIKit

class BaseFilterViewController: UIViewController {

    var filterNavigationView : FilterNavigationView?
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension BaseFilterViewController : FilterNavigationViewDelegate {
    func didClickApply() {
        log("didClickApply")
    }
}

extension BaseFilterViewController {
    func setUpView(){
        initFilterNavigationView()
    }

    func initFilterNavigationView(){
        filterNavigationView = FilterNavigationView.loadFromNibNamed("FilterNavigationView") as! FilterNavigationView?
        filterNavigationView?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 64)
        filterNavigationView?.delegate = self
        view.addSubview(filterNavigationView!)
    }
}
