//
//  ExtensionClasses.swift
//  ThuyKieu
//
//  Created by Dragon Member on 7/25/16.
//  Copyright © 2016 ThuyKieu LLC. All rights reserved.
//
import UIKit
import Foundation

extension Data {
  func toDictionary() -> [AnyHashable: Any]? {
    var error: NSError?
    let result: AnyObject?
    do {
      result = try JSONSerialization.jsonObject(with: self, options: .allowFragments) as! [String: AnyObject] as AnyObject?
      return result as? [AnyHashable: Any]
    } catch let err as NSError {
      error = err
      print(error!);
      return nil;
    }
  }
}

extension NSError {
  //For Client Error
  func isNoInternetConnectionError() -> Bool {
    return (self.domain == NSURLErrorDomain && (self.code == NSURLErrorNotConnectedToInternet || self.code == NSURLErrorNetworkConnectionLost || self.code == NSURLErrorCannotConnectToHost));
  }

  func isRequestTimeOutError() -> Bool {
    return self.code == NSURLErrorTimedOut
  }
}

extension HTTPURLResponse {
  func isServerNotFound() -> Bool {
    return self.statusCode == 404
  }

  func isInternalError() -> Bool {
    return self.statusCode == 500
  }

  func isErrorCode400() -> Bool {
    return self.statusCode == 400
  }
}

extension NSDictionary {
  func dictionaryToString() -> String {
    do {
      let data = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted) as Data?
      if let da = data {
        let strData = NSString(data: da, encoding: String.Encoding.utf8.rawValue)
        if let str = strData {
          var tempStr : String = str as String;
          tempStr = tempStr.replacingOccurrences(of: "\n", with: "")
          return tempStr;
        }
      }
    } catch let err as NSError {
      print(err);
      return ""
    }
    return ""
  }
}

extension UITextField {
  func disableAutoCorrectTextField() {
    return autocorrectionType = .no
  }
}
