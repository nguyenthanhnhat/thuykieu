
import UIKit

class BaseViewController: UIViewController, UIGestureRecognizerDelegate, CustomizeNavigationViewDelegate {

    let backButton: UIButton = UIButton()
    let titleBarLabel: UILabel = UILabel()
    let selectAccountButton: UIButton = UIButton()
    var infoButton: UIButton = UIButton()
    var customizeNavigationView : CustomizeNavigationView?


    var mainNavigationView : MainNavigationView?
    var menuAccountItems : [KxMenuItem] = []
    var toolBarView : ToolBarView?
    let rightView = UIView(frame: CGRect(x: 0, y: 0, width: 83, height: 44))

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpBaseView()
    }

    func didClickBack() {
        print("back ne")
    }

    func didClickShare() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func clickBack() {
        _ = navigationController?.popViewController(animated: true)
    }


    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension BaseViewController : MainNavigationViewDelegate {

    func initCustomNavigationView(){
        customizeNavigationView = CustomizeNavigationView.loadFromNibNamed("CustomizeNavigationView") as? CustomizeNavigationView
        customizeNavigationView?.delegate = self
        customizeNavigationView?.shareButton.isHidden = true
        customizeNavigationView?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 64)
        view.addSubview(customizeNavigationView!)
    }

    func didClickMainMenu() {
        toggleLeft()
    }
    
    func didClickUserMenu() {
        print("NTN")
    }

}

extension BaseViewController {
    func setUpBaseView() {
        self.view.backgroundColor = AppColors.backgroundPageBar
    }

    func addEventTouchInView () -> Void {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.touchInView))
        self.view.addGestureRecognizer(gesture)
    }

    @objc func touchInView(_ sender:UITapGestureRecognizer) {
        closeKeyBoard()
    }

    func closeKeyBoard()  {
        self.view.endEditing(true)
    }

}
