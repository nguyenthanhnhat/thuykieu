//
//  GeneralFunction.swift
//  ThuyKieu
//
//  Created by Rich Nguyen on 7/20/16.
//  Copyright © 2016 ThuyKieu LLC. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

let GET_APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
let MODEL_NAME = UIDevice.current.modelName

func delayOnMainQueue(_ delay: Double, closure: @escaping ()->()) {
    let when = DispatchTime.now() + delay // change 2 to desired number of seconds
    DispatchQueue.main.asyncAfter(deadline: when) {
        closure()
    }
}

//var  indicatorProgress : GradientCircularProgress?
func getDevEnviroment()->Bool{
    return Bundle.main.object(forInfoDictionaryKey: "DevEnviroment") as! Bool
}
func getLogFile()->Bool{
    return Bundle.main.object(forInfoDictionaryKey: "GetLog") as! Bool
}
func log(_ message: String, function: String = #function, file: String = #file, line: Int = #line) {
    if(getLogFile()){
        writeLog("\(Date()) \(file) \(function) \(line) \(message)")
    }
    //if(getDevEnviroment()){
//        print("\(message)")
    //}
}

func initViewController(_ storyboard: String, storyid: String) -> (UIViewController) {
    let storyboard = UIStoryboard(name: storyboard, bundle: nil) ;
    let viewController = storyboard.instantiateViewController(withIdentifier: storyid) ;
    return viewController;
}

func nullToNil(_ value : AnyObject?) -> AnyObject? {
    if value is NSNull {
        return nil
    } else {
        return value
    }
}

func SetToUserDefault(_ value: AnyObject?, key: String) {
    let defaults = UserDefaults.standard
    defaults.set(value, forKey: key)
    defaults.synchronize()
}

func GetFromUserDefault(_ key: String) -> AnyObject? {
    let defaults = UserDefaults.standard
    return defaults.object(forKey: key) as AnyObject?
}



func isValidEmail(_ testStr: String) -> Bool {
//    let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}


func isCheckNumber(_ number : String) -> Bool {
    return matchesForRegexInText("[0-9]", text: number).count > 0
}

func isCheckUperCase(_ str :String) ->Bool  {
     return matchesForRegexInText("[A-Z]", text: str).count>0
}


func isCheckLength(_ strLength : String) -> Bool {
    if strLength.characters.count > 5 {
        return true
    }else {
        return false
    }
}

func showAlertCustom(_ sender: UIViewController, title : String , message : String , buttonFirst: String, buttonSecond : String, completionHandleFirst: @escaping () -> (),  completionHandleSecond : @escaping () -> ()) {
    
    Popups.SharedInstance.ShowAlert(sender, title: title, message: message, buttons: [buttonFirst, buttonSecond]) {(buttonPressed) -> Void in
        if buttonPressed == buttonFirst {
            completionHandleFirst()
        }else if buttonPressed == buttonSecond{
            completionHandleSecond()
        }
    }
    
}

func showAlertCustomOneButton(_ sender: UIViewController, title : String , message : String , buttonFirst: String , completionHandle : @escaping () -> ()) {
    
    Popups.SharedInstance.ShowAlert(sender, title: title, message: message, buttons: [buttonFirst]) {(buttonPressed) -> Void in
        completionHandle()
    }

}


func matchesForRegexInText(_ regex: String, text: String) -> [String] {
    do {
        let regex = try NSRegularExpression(pattern: regex, options: [])
        let nsString = text as NSString
        let results = regex.matches(in: text,
                                            options: [], range: NSMakeRange(0, nsString.length))
        return results.map { nsString.substring(with: $0.range)}
    } catch let error as NSError {
        print("invalid regex: \(error.localizedDescription)")
        return []
    }
}

func getUsernameFromEmail(email : String) -> String {
    let range: Range<String.Index> = email.range(of: "@")!
    let index1: Int = email.distance(from: email.startIndex, to: range.lowerBound)
    let index = email.index(email.startIndex, offsetBy: index1)
    return email.substring(to: index)
}

func connectInternet() -> Bool{
    let status = Reach().connectionStatus()
    switch status {
        case .offline, .unknown:
            return false
        default :
            return true
    }
}


func viewController(_ view: UIView) -> UIViewController {
    var responder: UIResponder? = view
    while !(responder is UIViewController) {
        responder = responder?.next
        if nil == responder {
            break
        }
    }
    return (responder as? UIViewController)!
}

func goToView(_ viewController : UIViewController){
    let nv = UINavigationController(rootViewController: viewController)
    nv.interactivePopGestureRecognizer!.isEnabled = false
    nv.isNavigationBarHidden = true
    GET_APP_DELEGATE.slideViewController?.changeMainViewController(nv, close: true)
}

func categoriseData(typeCate : TYPE_CATEGORIES) -> [CategoryObject] {
    let cateThuyKieu = DataAccount.shared().categoriesThuyKieu ?? []
    let cateSkGioiTinh = DataAccount.shared().categoriesSKhoeGioiTinh ?? []
    let cateTamSu = DataAccount.shared().categoriesTamSu ?? []
    let cateTinNong = DataAccount.shared().categoriesTinNong ?? []
    let cateVideo = DataAccount.shared().categoriesVideo ?? []
    let cateSubThuyKieu = DataAccount.shared().categoriesSubThuyKieu ?? []


    switch typeCate {
    case .THUYKIEU :
        return cateThuyKieu
    case .SUB_THUYKIEU :
        return cateSubThuyKieu
    case .SKHOE_GTINH :
        return cateSkGioiTinh
    case .TAMSU :
        return cateTamSu
    case .TIN_NONG :
        return cateTinNong
    case .VIDEO :
        return cateVideo
    }
}


func getViewController(_ view: UIView) -> UIViewController {
    var responder: UIResponder? = view
    while !(responder is UIViewController) {
        responder = responder?.next
        if nil == responder {
            break
        }
    }
    return (responder as? UIViewController)!
}


func getImageShareFromServer(url : String) -> UIImage {
    var result : UIImage = UIImage()
    let url = URL(string: url)
    let data = try? Data(contentsOf: url!)
    if  let image = data {
        result = UIImage(data: image) ?? #imageLiteral(resourceName: "helio_full_color")
    }
    return result
}

func clearCategoriseData(typeCate : TYPE_CATEGORIES) {
    switch typeCate {
    case .THUYKIEU :
        DataAccount.shared().categoriesThuyKieu = []
        break
    case .SKHOE_GTINH :
        DataAccount.shared().categoriesSKhoeGioiTinh = []
        break
    case .TAMSU :
        DataAccount.shared().categoriesTamSu = []
        break
    case .TIN_NONG :
        DataAccount.shared().categoriesTinNong = []
        break
    case .VIDEO :
        DataAccount.shared().categoriesVideo = []
        break
    case .SUB_THUYKIEU :
        DataAccount.shared().categoriesSubThuyKieu = []
        break
    }
}

func getUrlGetCategories(id : String, typeCate : TYPE_CATEGORIES) -> String {
    switch typeCate {
    case .THUYKIEU :
        return UrlMethod.GET_SUB_CATEGORIES
    case .SKHOE_GTINH :
        return UrlMethod.GET_SUB_CHANEL_BY_ID + CategoryValue.SkhoeGtinh
    case .TAMSU :
        return UrlMethod.GET_SUB_CHANEL_BY_ID + CategoryValue.TamSu
    case .TIN_NONG :
        return UrlMethod.GET_SUB_CHANEL_BY_ID + CategoryValue.TinNong
    case .VIDEO :
        return UrlMethod.GET_SUB_CHANEL_BY_ID + CategoryValue.Video
    case .SUB_THUYKIEU :
        return UrlMethod.GET_SUB_CHANEL_BY_ID + id
    }
}

func addCategoriseData(typeCate : TYPE_CATEGORIES, cateSubObject : CategoryObject) {
    switch typeCate {
    case .THUYKIEU :
        DataAccount.shared().categoriesThuyKieu?.append(cateSubObject)
        break
    case .SKHOE_GTINH :
        DataAccount.shared().categoriesSKhoeGioiTinh?.append(cateSubObject)
        break
    case .TAMSU :
        DataAccount.shared().categoriesTamSu?.append(cateSubObject)
        break
    case .TIN_NONG :
        DataAccount.shared().categoriesTinNong?.append(cateSubObject)
        break
    case .VIDEO :
        DataAccount.shared().categoriesVideo?.append(cateSubObject)
        break
    case .SUB_THUYKIEU :
        DataAccount.shared().categoriesSubThuyKieu?.append(cateSubObject)
        break
    }
}

func setBoderButton(button : UIButton, color : CGColor ) -> UIButton{
    button.layer.cornerRadius = 3
    button.layer.borderWidth = 1
    button.layer.borderColor = color
    return button
}

func replaceCaption(str : String) -> String {
    var result = ""
    let idexStart = str.characters.index(of: "&")
    let idexEnd = str.characters.index(of: ";")
    if idexStart != nil && idexEnd != nil {
        let start = str.index(idexStart!, offsetBy: 0)
        let end = str.index(idexEnd!, offsetBy: 1)
        let range = start..<end
        result = str.replacingOccurrences(of: str[range], with: "", options: NSString.CompareOptions.literal, range: nil)
    }else {
        result = str
    }
    return result
}
