


import UIKit
import Pager

class PagerBaseViewController: PagerController, PagerDataSource {
    var mainNavigationView : MainNavigationView?
    var menuAccountItems : [KxMenuItem] = []
    var customizeNavigationView : CustomizeNavigationView?
    var toolBarView : ToolBarView?
    override func viewDidLoad() {
        super.viewDidLoad()
        initMainNavigationView()
    }
    var tabNamesCount = 0
    func loadData(tabNames: [String], tabControllers: [UIViewController]){
        self.dataSource = self
        tabNamesCount = tabNames.count
        self.setupPager(
            tabNames: tabNames,
            tabControllers: tabControllers)

        customizeTab()
    }

    func customizeTab() {
        indicatorColor = .red
        tabsViewBackgroundColor = AppColors.backgroundPageBar
        contentViewBackgroundColor = AppColors.backgroundMainColor

        startFromSecondTab = false
        centerCurrentTab = true
        tabLocation = PagerTabLocation.top
        tabHeight = 36
        tabOffset = 36
        var width : CGFloat = 140.0
        switch tabNamesCount {
        case 3:
            width = self.view.bounds.width/3
            break
        case 2:
            width = self.view.bounds.width/2
            break
        default:
            break
        }
        
        tabWidth = width
        fixFormerTabsPositions = false
        fixLaterTabsPosition = false
        animation = PagerAnimation.during
        selectedTabTextColor = UIColor.red
        tabsTextFont = UIFont(name: "HelveticaNeue", size: 16)!
    }
    

    func changeTab(index: Int) {
        self.selectTabAtIndex(index)
    }
}

extension PagerBaseViewController : MainNavigationViewDelegate {

    func initMainNavigationView(){
        mainNavigationView = MainNavigationView.loadFromNibNamed("MainNavigationView") as? MainNavigationView
        mainNavigationView?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 64)
        mainNavigationView?.delegate = self
        view.addSubview(mainNavigationView!)
    }

    func didClickMainMenu() {
        toggleLeft()
    }

    func showAccountMenu() {
        KxMenu.setTitleFont(UIFont(name: AppFonts.oxygenRegular, size: 15 ))
        var frame = mainNavigationView?.userMenuButton.frame
        frame!.origin.y = (frame?.origin.y)! - 10
        KxMenu.show(in: self.view, from: (frame)!, menuItems: menuAccountItems)
        KxMenu.setTintColor(UIColor.white)
    }

    func didClickUserMenu() {
        if menuAccountItems.count == 0{
            initAccountMenuData()
        }
        showAccountMenu()
    }

    func initAccountMenuData() {
        menuAccountItems = [
            KxMenuItem(USER_MENU.LOGOUT, image: UIImage(named : "f"), target: self, action:#selector(self.clickCreateAccount)),
        ]
        switch DataAccount.shared().typeUser {
        case .ThuyKieuUser:
            menuAccountItems  = [
                KxMenuItem(USER_MENU.LOGOUT, image: UIImage(named : "f"), target: self, action:#selector(self.clickCreateAccount)),

            ]
            break
        case .ThuyKieuUser:
            menuAccountItems.remove(at: 1)
            break
        default:
            break
        }
    }

    func clickMyAccount(sender : Any) {
//        log("clickMyAccount")
//        pushToMyAccount()
    }
    func clickHelioCard(sender : Any) {
//        log("clickHelioCard")
//        pushToMyHelioCard()
    }
    func clickSendFeedBack(sender : Any) {
//        log("clickSendFeedBack")
//        pushToFeedBack()
    }
    func clickNotification(sender : Any) {
//        log("clickNotification")
//        pushToNotification()
    }
    func clickSignIn(sender : Any) {
//        log("clickSignIn")
//        setSignInViewController()
    }
    @objc func clickCreateAccount(sender : Any) {
        GET_APP_DELEGATE.setWelcomeToRootViewController()
    }
    
}
