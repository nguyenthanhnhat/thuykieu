

import UIKit
import Pager

class RecentlyViewController: PagerBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
}

extension RecentlyViewController {
    func setUpView(){
        let storyboard = UIStoryboard(name: RECENTLY_STORYBOARD, bundle: nil)
        let recentlyOne = storyboard.instantiateViewController(withIdentifier: RECENTLY_ONE_VIEW_CONTROLER)
        let recentlyTwo = storyboard.instantiateViewController(withIdentifier: RECENTLY_TWO_VIEW_CONTROLER)
        let recentlyThree = storyboard.instantiateViewController(withIdentifier: RECENTLY_THREE_VIEW_CONTROLER)
        self.loadData(tabNames: ["RecentlyA".localized(), "RecentlyB".localized(), "RecentlyC".localized()],
                      tabControllers: [recentlyOne, recentlyTwo, recentlyThree])
    }
}
