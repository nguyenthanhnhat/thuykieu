


import UIKit
import FBSDKShareKit
import FBSDKLoginKit

class MainMenuViewController: BaseViewController {

    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var logoApp: UIImageView!

    var listItem : Array<ItemLeftMenu> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initLeftItems()
        setUpView()
        goToMainBar(index: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func homeAction(_ sender: UIButton) {
        CategoriesService.callGetCategories()
        goToMainBar(index: 0)
    }

}

extension MainMenuViewController : UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MAIN_MENU_CELL, for: indexPath) as! MainMenuCell
        let item = listItem[indexPath.row]
        cell.tittleLabel.text = item.nameLabel
        cell.iconImageView.image = UIImage(named: item.imageIcon)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let nearReadVc = NearlyReadViewController()
            goToView(nearReadVc)
            break
        case 1:
            let content = FBSDKAppInviteContent()
            content.appLinkURL = NSURL(string: "https://test/myapplink") as URL!
            content.appInvitePreviewImageURL = NSURL(string: "https://test/myapplink") as URL!
            FBSDKAppInviteDialog.show(from: self, with: content, delegate: self)
            break
        case 2:
            let settingVc = initViewController(LEFT_MENU_STORYBOARD, storyid: SETTING_VIEW_CONTROLER)
            goToView(settingVc)
            break
        case 3:
            let settingVc = initViewController(LEFT_MENU_STORYBOARD, storyid: CONTACTS_VIEW_CONTROLER)
            goToView(settingVc)
            break
        default:
            break
        }
    }
}

extension MainMenuViewController {
    func setUpView() {
        view.backgroundColor = AppColors.backgroundLogoLeftMenu
        menuTableView.backgroundColor = AppColors.backgroundLeftMenu
        menuTableView.separatorStyle = .none
        menuTableView.registerNib(nibName: MAIN_MENU_XIB, identifier: MAIN_MENU_CELL)
        menuTableView.reloadData()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(MainMenuViewController.didClickLogo))
        logoApp.isUserInteractionEnabled = true
        logoApp.addGestureRecognizer(gesture)
    }

    @objc func didClickLogo() {
        CategoriesService.callGetCategories()
        goToMainBar(index: 0)
    }

    func goToMainBar(index: Int) {
        let memberMainPageVc = MemberMainPageViewController()
        delayOnMainQueue(0.1) {
            memberMainPageVc.changeTab(index: index)
        }
        changeMainViewController(memberMainPageVc)
    }

    func initLeftItems(){
        let readNearlyItem      = ItemLeftMenu(label: "Đọc gần đây".localized(), image: "time_past_icon")
        let inviteFriendsItem   = ItemLeftMenu(label: "Mời bạn bè sử dụng".localized(), image: "invite_icon")
        let settingItem         = ItemLeftMenu(label: "Cài Đặt".localized(), image: "setting_icon")
        let contactUs           = ItemLeftMenu(label: "Liên hệ".localized(), image: "setting_icon")

        listItem.append(readNearlyItem)
        listItem.append(inviteFriendsItem)
        listItem.append(settingItem)
        listItem.append(contactUs)
    }

    func didClickReadNearly(index: Int){
        let recentlyVc = RecentlyViewController()
        delayOnMainQueue(0.1) {
            recentlyVc.changeTab(index: index)
        }
        changeMainViewController(recentlyVc)
    }
}

extension MainMenuViewController : FBSDKAppInviteDialogDelegate {

    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable : Any]!) {
        print("******NTN********")
    }

    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Error!) {
        print("******NTN********")
    }
}
