
import UIKit
import Pager

class MemberMainPageViewController: PagerBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
}

extension MemberMainPageViewController {
    func setUpView() {
        let storyboard      = UIStoryboard(name: MENU_PAGE_STORYBOARD, bundle: nil)

        let thuykieuVc     = storyboard.instantiateViewController(withIdentifier: THUYKIEU_VIEW_CONTROLER)
        let skhoeGtVc    = storyboard.instantiateViewController(withIdentifier: SUCKHOE_GIOITINH_VIEW_CONTROLER)
        let tinNongVc  = storyboard.instantiateViewController(withIdentifier: TIN_NONG_VIEW_CONTROLER)
        let tamsuVc = storyboard.instantiateViewController(withIdentifier: TAMSU_VIEW_CONTROLER)
        let videoVc = storyboard.instantiateViewController(withIdentifier: VIDEO_VIEW_CONTROLER)


        if let categories = DataAccount.shared().categories , categories.count > 4 {
            self.loadData(tabNames:  [categories[0].name, categories[1].name, categories[2].name, categories[3].name, categories[4].name], tabControllers: [thuykieuVc, skhoeGtVc, tinNongVc, tamsuVc, videoVc])
        }else {
         self.loadData(tabNames:  ["Thuý Kiều".localized(), "Sức Khoẻ Giới tính".localized(), "Tin Nóng".localized(), "Tâm Sự".localized(), "video".localized()], tabControllers: [thuykieuVc, skhoeGtVc, tinNongVc, tamsuVc, videoVc])
        }


    }
}
