


import UIKit

class HomeViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func clickBackHome(_ sender: Any) {
        GET_APP_DELEGATE.setWelcomeToRootViewController()
    }

}

extension HomeViewController {
    func setUpView() {
        initCustomNavigationView()
    }
}
