

import UIKit

class ChapModuleCell: UITableViewCell {

    @IBOutlet weak var supperChapView: UIView!
    @IBOutlet weak var chapImageView        : UIImageView!
    @IBOutlet weak var titleChapLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

extension ChapModuleCell {
    func setUpView() {

    }

}
