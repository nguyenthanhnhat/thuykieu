
import UIKit

class MainMenuCell: UITableViewCell {

    @IBOutlet weak var tittleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension MainMenuCell {
    func setUpView() {
        self.backgroundColor = AppColors.backgroundLeftMenu
        self.contentView.backgroundColor = AppColors.backgroundLeftMenu
    }
}
