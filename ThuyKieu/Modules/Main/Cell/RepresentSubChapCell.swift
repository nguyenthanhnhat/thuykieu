

import UIKit

@objc protocol RepresentDelegate : class {
    @objc optional func didClickBackTime()
    @objc optional func didClickVideo()
    @objc optional func didClickThoiSu()
}

class RepresentSubChapCell: UITableViewCell {
    @IBOutlet weak var representImage: UIImageView!
    @IBOutlet weak var nameTitleLabel: UILabel!
    @IBOutlet weak var ThoiSuButton: UIButton!
    @IBOutlet weak var timeButton: UIButton!
    @IBOutlet weak var videoButton: UIButton!

    weak var peresentDelegate : RepresentDelegate?

    override func awakeFromNib() {
        timeButton.isHidden = true
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    @IBAction func didClickBakTime(_ sender: Any) {
        peresentDelegate?.didClickBackTime!()
    }

    @IBAction func didClickVideo(_ sender: Any) {
        peresentDelegate?.didClickVideo!()
    }

    @IBAction func didClickThoiSu(_ sender: Any) {
        peresentDelegate?.didClickThoiSu!()
    }

}
