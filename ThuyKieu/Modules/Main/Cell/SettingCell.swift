
import UIKit

class SettingCell: UITableViewCell {
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var settingNameLabel: UILabel!
    @IBOutlet weak var statusSwich: UISwitch!

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

extension SettingCell {
    func setUpView() {
        statusSwich.isOn = false
    }
}
