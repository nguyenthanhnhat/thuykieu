
import Foundation

class ItemLeftMenu {
    var imageIcon: String = ""
    var nameLabel: String = ""

    init(label: String, image: String) {
        self.imageIcon = image
        self.nameLabel = label
    }
}
