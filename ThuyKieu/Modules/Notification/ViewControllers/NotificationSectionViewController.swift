//
//  NotificationSectionViewController.swift
//  Helio
//
//  Created by Rich Nguyen on 3/4/17.
//  Copyright © 2017 Helio Company. All rights reserved.
//

import UIKit

class NotificationSectionViewController: PagerBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Instantiating Storyboard ViewControllers
        mainNavigationView?.tittleLabel.text = "NOTIFICATION"
        let storyboard = UIStoryboard(name: "Notification", bundle: nil)
        let allVC = storyboard.instantiateViewController(withIdentifier: "AllNotificationViewController")
        let promotion = storyboard.instantiateViewController(withIdentifier: "PromotionViewController")
        let newsVC = storyboard.instantiateViewController(withIdentifier: "NewsViewController")
        
        // Setting up the PagerController with Name of the Tabs and their respective ViewControllers
        self.loadData(
            tabNames: ["All (10)", "Promotion (4)", "News (1)"],
            tabControllers: [allVC, promotion, newsVC])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
