
import UIKit

class ReadNearlyViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension ReadNearlyViewController {
    func setUpView() {
        initCustomNavigationView()
        mainNavigationView?.tittleLabel.text = "Read Nearly".localized()
    }
}
