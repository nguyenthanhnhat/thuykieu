
import UIKit

class NearlyReadViewController: BaseViewController {

    @IBOutlet weak var recentlyTableView: UITableView!
    var readNearLyData : [CategoryObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        initCustomNavigationView()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if readNearLyData.count == 0 {
            return
        }else {
            animateTableView()
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


extension NearlyReadViewController {
    func setUpView() {
        readNearLyData = DataAccount.shared().readNearlyChap!
        if readNearLyData.count == 0 {
            recentlyTableView.separatorStyle = .none
            recentlyTableView.isScrollEnabled = false
            recentlyTableView.allowsSelection = false
        }
        customizeNavigationView?.tittleLabel.text = "Đọc gần đây".localized()
        recentlyTableView.registerNib(nibName: REPRECENT_SUB_CHAP_CELL_NIB, identifier: REPRECENT_IDENTIFIER_CELL)
        recentlyTableView.registerNib(nibName: "NoDataCell", identifier: "noDataCell")
        recentlyTableView.delegate = self
        recentlyTableView.dataSource = self
    }

    func configureChapCell(representSubChapCell : RepresentSubChapCell) -> RepresentSubChapCell {
        representSubChapCell.representImage.image = UIImage(named: "Image1")
        representSubChapCell.backgroundColor = AppColors.backgroundRegisterColor
        representSubChapCell.nameTitleLabel.text = "Thuý Kiều rơi vào tay Mã Giám Sinh và Tú Bà".localized()
        return representSubChapCell
    }

//    override func didClickBack() {
//        self.toggleLeft()
//    }

    func animateTableView() {
        recentlyTableView.reloadData()
        let cells = recentlyTableView.visibleCells
        let tableHeight: CGFloat = recentlyTableView.bounds.size.height
        var index = 0

        for indexModule in cells {
            let cell: RepresentSubChapCell = indexModule as! RepresentSubChapCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        for indexCell in cells {
            let cell: RepresentSubChapCell = indexCell as! RepresentSubChapCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }

}

extension NearlyReadViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if readNearLyData.count == 0 {
            return 1
        }
        return readNearLyData.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if readNearLyData.count == 0 {
            return recentlyTableView.frame.height - 64
        }
        return 199

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if readNearLyData.count == 0 {
            return  recentlyTableView.dequeueReusableCell(withIdentifier: "noDataCell", for: indexPath) as! NoDataCell
        }
        let cell = recentlyTableView.dequeueReusableCell(withIdentifier: REPRECENT_IDENTIFIER_CELL, for: indexPath) as! RepresentSubChapCell
        return configureChapCell(representSubChapCell: cell)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let details = initViewController(MENU_PAGE_STORYBOARD, storyid: "ChapDetailsViewController")
        self.navigationController?.pushViewController(details, animated: true)
    }
}

