//
//  ContactUsViewController.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 6/28/17.
//  Copyright © 2017 vn. All rights reserved.
//

import UIKit

class ContactUsViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        initCustomNavigationView()
        customizeNavigationView?.tittleLabel.text = "Liên Hệ".localized()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func didClickBack() {
        self.toggleLeft()
    }
}
