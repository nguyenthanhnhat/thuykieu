
import UIKit

class SettingViewController: BaseViewController {
    @IBOutlet weak var settingTableView: UITableView!

    var downLoadImageCell   : SettingCell?
    var nightReadCell       : SettingCell?
    var autoPlayVideo       : SettingCell?
    var settings : [SettingCell] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension SettingViewController : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return settings[indexPath.row]
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("\(indexPath.row)")
    }

}

extension SettingViewController {
    func setUpView() {
        initCustomNavigationView()
        customizeNavigationView?.tittleLabel.text = "Setting".localized()
        settingTableView.delegate = self
        settingTableView.dataSource = self
        settingTableView.tableFooterView = UIView()
        settingTableView.allowsSelection = false
        settings = initCell()
    }

    func initSettingCell(imageName : String, settingName : String , settingStatus : Bool) -> SettingCell{
        let cell = Bundle.main.loadNibNamed("SettingCell", owner: self, options: nil)?[0] as! SettingCell
        cell.settingNameLabel.text = settingName
        cell.statusSwich.isOn = settingStatus
        cell.backgroundColor = AppColors.backgroundRegisterColor
        cell.iconImage.image = UIImage(named: imageName)
        return cell
    }

    func initCell() -> [SettingCell]{
        var settingList : [SettingCell] = []
        downLoadImageCell = initSettingCell(imageName: "download_image_icon", settingName: "Tải hình ảnh".localized(), settingStatus: true)
        nightReadCell    = initSettingCell(imageName: "readNight_icon", settingName: "Đọc ban đêm".localized(), settingStatus: false)
        autoPlayVideo    = initSettingCell(imageName: "download_image_icon", settingName: "Tự động chơi video".localized(), settingStatus: true)
        settingList.append(downLoadImageCell!)
        settingList.append(nightReadCell!)
        settingList.append(autoPlayVideo!)
        return settingList
    }

//    override func didClickBack() {
//        self.toggleLeft()
//    }
}
