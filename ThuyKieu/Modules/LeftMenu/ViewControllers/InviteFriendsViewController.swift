
import UIKit
import FBSDKShareKit
import FBSDKLoginKit

class InviteFriendsViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension InviteFriendsViewController : FBSDKAppInviteDialogDelegate {

    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable : Any]!) {
        print("******NTN********")
    }

    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Error!) {
        print("******NTN********")
    }
}

extension InviteFriendsViewController {
    func setUpView() {
        initCustomNavigationView()
        customizeNavigationView?.tittleLabel.text = "Invite Friends".localized()
        let content = FBSDKAppInviteContent()
        content.appLinkURL = NSURL(string: "https://test/myapplink") as URL!
        content.appInvitePreviewImageURL = NSURL(string: "https://test/myapplink") as URL!
        FBSDKAppInviteDialog.show(from: self, with: content, delegate: self)
    }

//    override func didClickBack() {
//        self.toggleLeft()
//    }
}


