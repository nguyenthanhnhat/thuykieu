//
//  LevelObject.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 6/4/17.
//  Copyright © 2017 vn. All rights reserved.
//

import Foundation

class LevelObject: NSObject, NSCopying {

    var level_id           : Int
    var label              : String
    var slug               : String


    override init() {
        self.level_id = 0
        self.label = ""
        self.slug = ""
    }

    init(level_id : Int, label : String, slug : String) {
        self.level_id = level_id
        self.label = label
        self.slug = slug
    }

    func copy(with zone: NSZone?) -> Any {
        let copy = LevelObject(
            level_id                    : level_id,
            label                       : label,
            slug                        :  slug
        )
        return copy
    }
}
