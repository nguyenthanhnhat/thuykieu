//
//  postDetails.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 6/6/17.
//  Copyright © 2017 vn. All rights reserved.
//

import Foundation


class PostDetails: NSObject, NSCopying {

    var id              : Int
    var title           : String
    var content         : String
    var image           : String
    var date            : String


    override init() {
        self.id = 0
        self.title = ""
        self.content = ""
        self.image = ""
        self.date = ""
    }

    init(id : Int, title : String, content : String, image : String , date : String) {
        self.id = id
        self.title = title
        self.content = content
        self.image = image
        self.date = date
    }

    func copy(with zone: NSZone?) -> Any {
        let copy = PostDetails(
            id                    : id,
            title                 : title,
            content               :  content,
            image                 : image,
            date                  : date
        )
        return copy
    }
}
