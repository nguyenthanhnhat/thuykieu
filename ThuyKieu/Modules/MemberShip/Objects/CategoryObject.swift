//
//  CategoryObject.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 6/4/17.
//  Copyright © 2017 vn. All rights reserved.
//

import Foundation

class CategoryObject: NSObject, NSCopying {

    var id                  : String
    var name                : String
    var image               : UIImage
    var level               : String
    var content             : String
    var nameLevel       : String


    override init() {
        self.id = ""
        self.name = ""
        self.image = UIImage(named : "Image1")!
        self.level = ""
        self.content = ""
        self.nameLevel = "Khách lạ"
    }

    init(id : String, name : String, image : UIImage, level : String, content : String, nameLevel : String) {
        self.id = id
        self.name = name
        self.level =  level
        self.image = image
        self.content = content
        self.nameLevel = nameLevel
    }

    func copy(with zone: NSZone?) -> Any {
        let copy = CategoryObject(
            id                  : id,
            name                : name,
            image               : image,
            level               : level,
            content             : content,
            nameLevel           : nameLevel
        )
        return copy
    }
}
