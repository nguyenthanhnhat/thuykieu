//
//  CategoriesService.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 6/4/17.
//  Copyright © 2017 vn. All rights reserved.
//

enum TYPE_CATEGORIES{
    case THUYKIEU
    case SUB_THUYKIEU
    case SKHOE_GTINH
    case TAMSU
    case TIN_NONG
    case VIDEO
}


import Foundation
import UIKit

class CategoriesService {
    
    class func callGetCategories(completionHandler: @escaping (Dictionary<String, AnyObject>?) -> ()) {
        CoreServices.share.callThuykieuService(KeyHttpMethod.GET, urlString: UrlMethod.GET_CATEGORIES, bodyRequest: nil) { (dictionary) in
            completionHandler(dictionary)
        }
    }

    class func updateLevel(completionHandler: @escaping (Dictionary<String, AnyObject>?) -> ()) {
        let url = "http://thuykieu.com/api/user/update_level/?insecure=cool&user_id=" + (DataAccount.shared().userInfo?.id)! + "&level_id=1"
        CoreServices.share.callThuykieuService(KeyHttpMethod.GET, urlString: url, bodyRequest: nil) { (dictionary) in
            completionHandler(dictionary)
        }
    }



    class func chartPaymentUpdateLevel(numberCard : String, month : String , year : String, cvv : String, amount : String, completionHandler: @escaping (Dictionary<String,AnyObject>?) -> ()) {
        let url = "http://thuykieu.com/api/user/stripe/?insecure=cool&number=\(numberCard)&exp_month=\(month)&exp_year=\(year)&cvc=\(cvv)&amount=\(amount)"
        CoreServices.share.callThuykieuService(KeyHttpMethod.GET, urlString: url, bodyRequest: nil) { (dictionary) in
            completionHandler(dictionary)
        }
    }

    class func callGetSubCategories(id : String, typeCate : TYPE_CATEGORIES, completionHandler: @escaping (Dictionary<String, AnyObject>?) -> ()) {
        let url = getUrlGetCategories(id : id, typeCate : typeCate)
        CoreServices.share.callThuykieuService(KeyHttpMethod.GET, urlString: url, bodyRequest: nil) { (dictionary) in
            completionHandler(dictionary)
        }
    }


    class func callGetSubchanelId(isCallSsubThuyKieu  : Bool, id : String, completionHandler: @escaping (Dictionary<String, AnyObject>?) -> ()) {
        let url = UrlMethod.GET_SUB_CHANEL_BY_ID + id
        if !isCallSsubThuyKieu {
            DataAccount.shared().categoriesById = []
        }else {
            DataAccount.shared().categoriesSubThuyKieu = []
        }

        CoreServices.share.callThuykieuService(KeyHttpMethod.GET, urlString: url, bodyRequest: nil) { (dictionary) in
            completionHandler(dictionary)
            log("dic \(String(describing: dictionary))")
            let dic = JSON(dictionary!).object
            let data = dic?[DATA_RESPOND]?.object
            let status = data?[STATUS]?.string
            if status == "ok" {
                if let categories = data?["categories"]?.array , categories.count > 0 {
                    for cate in categories {
                        let cateByIdObject = self.parseCategoryObject(categoryJsonObject: cate)
                        if !isCallSsubThuyKieu {
                            DataAccount.shared().categoriesById?.append(cateByIdObject)
                        }else {
                            DataAccount.shared().categoriesSubThuyKieu?.append(cateByIdObject)
                        }

                    }
                }
            }
            return
        }
    }

    class func callGetChapCategoriesByIdCategory(id : String, typeCate : TYPE_CATEGORIES, completionHandler: @escaping (Dictionary<String, AnyObject>?) -> ()) {
        CategoriesService.callGetSubCategories(id : id, typeCate : typeCate) { (dictionary) in
            log("dic \(typeCate) \(String(describing: dictionary))")
            let dic = JSON(dictionary!).object
            let data = dic?[DATA_RESPOND]?.object
            let status = data?[STATUS]?.string
            if status == "ok" {
                if let cateChaps = data?["posts"]?.array , cateChaps.count > 0 {
                    clearCategoriseData(typeCate: typeCate)
                    for cate in cateChaps {
                        let cateSubObject = self.parseCategoryObject(categoryJsonObject: cate)
                        addCategoriseData(typeCate: typeCate, cateSubObject: cateSubObject)
                    }
                    completionHandler(dictionary)
                }
            }
            return
        }
    }


    class func callGetChapParentThuyKieuByIdParent(typeCate : TYPE_CATEGORIES, completionHandler: @escaping (Dictionary<String, AnyObject>?) -> ()) {
        CategoriesService.callGetSubCategories(id : "", typeCate : typeCate) { (dictionary) in
            log("dic \(typeCate) \(String(describing: dictionary))")
            let dic = JSON(dictionary!).object
            let data = dic?[DATA_RESPOND]?.object
            let status = data?[STATUS]?.string
            if status == "ok" {
                if let cateParentThuyKieu = data?["categories"]?.array , cateParentThuyKieu.count > 0 {
                    DataAccount.shared().categoriesThuyKieu?.removeAll()
                    DataAccount.shared().categoriesThuyKieu = []
                    for cate in cateParentThuyKieu {
                        let categoryObject  = CategoryObject()
                        categoryObject.id = String((cate["id"].integer)!)
                        categoryObject.name = cate["name"].string ?? ""
                        DataAccount.shared().categoriesThuyKieu?.append(categoryObject)
                    }
                    completionHandler(dictionary)
                }
            }
            return
        }
    }

    class func parseCategoryObject(categoryJsonObject : JSON) -> CategoryObject{
        let categoryObject  = CategoryObject()
        categoryObject.id = String(categoryJsonObject["id"].integer!)
        var title = categoryJsonObject["title"].string ?? ""
        title = replaceCaption(str : title)
        categoryObject.name = title
        categoryObject.level = (categoryJsonObject["custom_fields"]["ihc_mb_who"].array![0]).string ?? ""
        categoryObject.content = categoryJsonObject["content"].string ?? ""
        let image = categoryJsonObject["thumbnail"].string ?? ""
        if image != "" {
            categoryObject.image = getImageShareFromServer(url: image)
        }else {
            categoryObject.image = UIImage(named : "Image1")!
        }

        switch categoryObject.level {
        case "":
            categoryObject.nameLevel = "Khách Vãng Lai"
            break
        default:
            categoryObject.nameLevel = "Thượng Khách"
            break
        }
        return categoryObject
    }


    class func callGetListChapCategoryById(id : String ,typeCate : TYPE_CATEGORIES, completionHandler: @escaping (Dictionary<String, AnyObject>?) -> ()) {
        CategoriesService.callGetChapCategoriesByIdCategory(id : id, typeCate : typeCate) { (dictionary) in
            completionHandler(dictionary)
        }
    }

    class func callGetCategories() {
        GET_APP_DELEGATE.showActivityWithLabel(string: "Loading...")
        CategoriesService.callGetChapParentThuyKieuByIdParent(typeCate: .THUYKIEU) { (dictionary) in
            GET_APP_DELEGATE.hideActivity()
        }
//        GET_APP_DELEGATE.showActivityWithLabel(string: "Loading...")
//        CategoriesService.callGetListChapCategoryById(id : "", typeCate: .SKHOE_GTINH) { (dictionary) in
//            GET_APP_DELEGATE.hideActivity()
//        }
//        GET_APP_DELEGATE.showActivityWithLabel(string: "Loading...")
//        CategoriesService.callGetListChapCategoryById(id : "", typeCate: .TIN_NONG) { (dictionary) in
//            GET_APP_DELEGATE.hideActivity()
//        }
//        GET_APP_DELEGATE.showActivityWithLabel(string: "Loading...")
//        CategoriesService.callGetListChapCategoryById(id : "", typeCate: .TAMSU) { (dictionary) in
//            GET_APP_DELEGATE.hideActivity()
//        }
//        GET_APP_DELEGATE.showActivityWithLabel(string: "Loading...")
//        CategoriesService.callGetListChapCategoryById(id : "", typeCate: .VIDEO) { (dictionary) in
//            GET_APP_DELEGATE.hideActivity()
//        }
    }

}

