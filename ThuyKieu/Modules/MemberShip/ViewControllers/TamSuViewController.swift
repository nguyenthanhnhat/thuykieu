
import UIKit

class TamSuViewController: BaseMenuViewController {


    @IBOutlet weak var tSuTableView: UITableView!

    var tsuChaps : [CategoryObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        CategoriesService.callGetCategorySubChanel(typeCate: .TAMSU) { (dictionary) in
//            self.tSuTableView.reloadData()
//        }
        tSuTableView.reloadData()
        animateTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension TamSuViewController {

    func setUpView() {
        initTableView()
        tSuTableView.delegate = self
        tSuTableView.dataSource = self
        tsuChaps = DataAccount.shared().categoriesTamSu!
        if tsuChaps.count == 0 {
            tSuTableView.separatorStyle = .none
            tSuTableView.allowsSelection = false
        }
        tSuTableView.tableFooterView = UIView()
        tSuTableView.backgroundColor = AppColors.backgroundRegisterColor
        tSuTableView.registerNib(nibName: CHAP_MODULE_CELL_NIB, identifier: CHAP_MODULE_IDENTIFIER_CELL)
        tSuTableView.registerNib(nibName: REPRECENT_SUB_CHAP_CELL_NIB, identifier: REPRECENT_IDENTIFIER_CELL)
        tSuTableView.registerNib(nibName: "NoDataCell", identifier: "noDataCell")
        view.backgroundColor = AppColors.backgroundPageBar
        tSuTableView.backgroundColor = AppColors.backgroundPageBar
    }

    func initTableView() {
        tSuTableView.registerNib(nibName: CHAP_CELL_NIB, identifier: CHAP_IDENTIFIER_CELL)
        tSuTableView.delegate = self
        tSuTableView.dataSource = self
    }

    func animateTableView() {
        tSuTableView.reloadData()
        let cells = tSuTableView.visibleCells
        let tableHeight: CGFloat = tSuTableView.bounds.size.height
        var index = 0

        for indexModule in cells {
            let cell: UITableViewCell = indexModule
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        for indexCell in cells {
            let cell: UITableViewCell = indexCell 
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
}

extension TamSuViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tsuChaps.count == 1 {
            return 1
        }
        return tsuChaps.count

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tsuChaps.count == 0 {
            return view.frame.height
        }
        if tsuChaps.count == 1 {
            return 94
        }
        if indexPath.row == 0 {
            return 180
        }
        return 94
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tsuChaps.count == 0 {
            let cellRePresent = tSuTableView.dequeueReusableCell(withIdentifier: "noDataCell", for: indexPath) as! NoDataCell
            return cellRePresent
        }
        if tsuChaps.count == 1 {
            let cell  = tSuTableView.dequeueReusableCell(withIdentifier: CHAP_MODULE_IDENTIFIER_CELL, for: indexPath) as! ChapModuleCell
            cell.chapImageView.image = tsuChaps[indexPath.row].image
            cell.levelLabel.text = tsuChaps[indexPath.row].nameLevel
            cell.titleChapLabel.text = tsuChaps[indexPath.row].name
            cell.supperChapView.layer.cornerRadius = 5
            return cell
        }else {
            if indexPath.row == 0 {
                let cellRePresent = tSuTableView.dequeueReusableCell(withIdentifier: REPRECENT_IDENTIFIER_CELL, for: indexPath) as! RepresentSubChapCell
                cellRePresent.representImage.image = tsuChaps[indexPath.row].image
                cellRePresent.nameTitleLabel.text = tsuChaps[indexPath.row].name
                cellRePresent.ThoiSuButton.setTitle(tsuChaps[indexPath.row].nameLevel, for: .normal)
                cellRePresent.timeButton.setTitle("21 phút trước", for: .normal)
            }else {
                let cell  = tSuTableView.dequeueReusableCell(withIdentifier: CHAP_MODULE_IDENTIFIER_CELL, for: indexPath) as! ChapModuleCell
                cell.chapImageView.image = tsuChaps[indexPath.row].image
                cell.levelLabel.text = tsuChaps[indexPath.row].nameLevel
                cell.titleChapLabel.text = tsuChaps[indexPath.row].name
                cell.supperChapView.layer.cornerRadius = 5
                return cell
            }
        }
        return UITableViewCell()
    }

    func goToDetailView(cate : CategoryObject) {
        let details = initViewController(MENU_PAGE_STORYBOARD, storyid: "ChapDetailsViewController") as! ChapDetailsViewController
        details.header = cate.name
        details.content = cate.content
        details.titlePage = "Tâm Sự"
        self.pushViewControllerInTab(ViewController: details)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let levelPost = tsuChaps[indexPath.row].level
        let idLevel = DataAccount.shared().userInfo?.level ?? ""

        if levelPost == "" {
            goToDetailView(cate: tsuChaps[indexPath.row])
            return
        }else {
            if DataAccount.shared().typeUser == .ThuyKieuUser {
                if idLevel != "" {
                    goToDetailView(cate: tsuChaps[indexPath.row])
                    return
                }else {
                    showAlertCustom(self, title: "ThuyKieu", message: "Không thể truy cập , vui lòng nâng cấp account", buttonFirst: "OK", buttonSecond: "Cancel", completionHandleFirst: {
                        let vc = initViewController(REGISTER_STORYBOARD, storyid: "PaymentInfoViewController")
                        self.pushViewControllerInTab(ViewController: vc)
                    }, completionHandleSecond: {return})
                    return
                }
            }else {
                showAlertCustom(self, title: "ThuyKieu", message: "Không thể truy cập , vui lòng đăng kí tài khoản", buttonFirst: "OK", buttonSecond: "Cancel", completionHandleFirst: {
                    GET_APP_DELEGATE.setWelcomeToRootViewController()
                }, completionHandleSecond: {})
            }
        }
    }
}
