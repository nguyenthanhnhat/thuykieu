

import UIKit

class ThuyKieuViewController: BaseMenuViewController {

    @IBOutlet weak var thuyKieuChapTableView: UITableView!
    var listThuyKieuChap : [CategoryObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        CategoriesService.callGetCategorySubChanel(typeCate: .THUYKIEU) { (dictionary) in
//            self.thuyKieuChapTableView.reloadData()
//        }
        thuyKieuChapTableView.reloadData()
        animateTableView()
    }


    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension ThuyKieuViewController {
    func setupView() {

        listThuyKieuChap = DataAccount.shared().categoriesThuyKieu!
        if listThuyKieuChap.count == 0 {
            thuyKieuChapTableView.separatorStyle = .none
            thuyKieuChapTableView.allowsSelection = false
        }
        thuyKieuChapTableView.tableFooterView = UIView()
        thuyKieuChapTableView.delegate = self
        thuyKieuChapTableView.dataSource = self
        thuyKieuChapTableView.backgroundColor = AppColors.backgroundRegisterColor
        thuyKieuChapTableView.registerNib(nibName: CHAP_MODULE_CELL_NIB, identifier: CHAP_MODULE_IDENTIFIER_CELL)
        thuyKieuChapTableView.registerNib(nibName: "NoDataCell", identifier: "noDataCell")
        view.backgroundColor = AppColors.backgroundPageBar
        thuyKieuChapTableView.backgroundColor = AppColors.backgroundPageBar
    }

    func animateTableView() {
        thuyKieuChapTableView.reloadData()
        let cells = thuyKieuChapTableView.visibleCells
        let tableHeight: CGFloat = thuyKieuChapTableView.bounds.size.height
        var index = 0

        for indexModule in cells {
            let cell: UITableViewCell = indexModule
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        for indexCell in cells {
            let cell: UITableViewCell = indexCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
    
}

extension ThuyKieuViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if listThuyKieuChap.count == 0 {
            return 1
        }
        return listThuyKieuChap.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if listThuyKieuChap.count == 0 {
            return self.view.frame.height
        }
        return 120
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if listThuyKieuChap.count == 0 {
            let cellRePresent = thuyKieuChapTableView.dequeueReusableCell(withIdentifier: "noDataCell", for: indexPath) as! NoDataCell
            return cellRePresent
        }
        let cell = thuyKieuChapTableView.dequeueReusableCell(withIdentifier: CHAP_MODULE_IDENTIFIER_CELL, for: indexPath) as! ChapModuleCell
        cell.titleChapLabel.text = listThuyKieuChap[indexPath.row].name
        cell.chapImageView.image = listThuyKieuChap[indexPath.row].image
        cell.levelLabel.text = listThuyKieuChap[indexPath.row].level
        cell.supperChapView.layer.cornerRadius = 5
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = listThuyKieuChap[indexPath.row].id
        GET_APP_DELEGATE.showActivityWithLabel(string: "Loading...")
        CategoriesService.callGetListChapCategoryById(id : index, typeCate: .SUB_THUYKIEU) { (dictionary) in
            GET_APP_DELEGATE.hideActivity()
            let vc = initViewController(MENU_PAGE_STORYBOARD, storyid: SUB_CHAP_THUYKIEU_VIEW_CONTROLER) as! SubChapThuyKieuViewController
            vc.titlePage = self.listThuyKieuChap[indexPath.row].name
            self.pushViewControllerInTab(ViewController: vc)

        }
    }
    
}




