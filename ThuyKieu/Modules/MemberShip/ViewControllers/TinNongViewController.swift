

import UIKit

class TinNongViewController: BaseMenuViewController {

    @IBOutlet weak var hotNewsTableView: UITableView!
    var hotNewcategories : [CategoryObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        CategoriesService.callGetCategorySubChanel(typeCate: .TIN_NONG) { (dictionary) in
//            self.hotNewsTableView.reloadData()
//        }
//        if hotNewcategories.count == 0 {
//            return
//        }else {
        hotNewsTableView.reloadData()
            animateTableView()
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension TinNongViewController {

    func setUpView() {
        initTableView()
        hotNewcategories = DataAccount.shared().categoriesTinNong!
        if hotNewcategories.count == 0 {
            hotNewsTableView.separatorStyle = .none
            hotNewsTableView.allowsSelection = false
        }
        hotNewsTableView.tableFooterView = UIView()
        hotNewsTableView.delegate = self
        hotNewsTableView.dataSource = self
        hotNewsTableView.backgroundColor = AppColors.backgroundRegisterColor
        hotNewsTableView.registerNib(nibName: CHAP_MODULE_CELL_NIB, identifier: CHAP_MODULE_IDENTIFIER_CELL)
        hotNewsTableView.registerNib(nibName: REPRECENT_SUB_CHAP_CELL_NIB, identifier: REPRECENT_IDENTIFIER_CELL)
        hotNewsTableView.registerNib(nibName: "NoDataCell", identifier: "noDataCell")
        view.backgroundColor = AppColors.backgroundPageBar
        hotNewsTableView.backgroundColor = AppColors.backgroundPageBar
    }

    func initTableView() {
        let heightNavi : CGFloat = 0
        hotNewsTableView.registerNib(nibName: CHAP_CELL_NIB, identifier: CHAP_IDENTIFIER_CELL)
        hotNewsTableView.frame = CGRect(x: 0, y: heightNavi, width: view.frame.width, height: view.frame.height - heightNavi)
        hotNewsTableView.delegate = self
        hotNewsTableView.dataSource = self
    }


    func animateTableView() {
        hotNewsTableView.reloadData()
        let cells = hotNewsTableView.visibleCells
        let tableHeight: CGFloat = hotNewsTableView.bounds.size.height
        var index = 0

        for indexModule in cells {
            let cell: UITableViewCell = indexModule
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        for indexCell in cells {
            let cell: UITableViewCell = indexCell 
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
}

extension TinNongViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if hotNewcategories.count == 0 {
            return 1
        }
        return hotNewcategories.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if hotNewcategories.count == 0 {
            return self.view.frame.height
        }
        if hotNewcategories.count == 1 {
            return 94
        }
        if indexPath.row == 0 {
            return 180
        }
        return 94
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if hotNewcategories.count == 0 {
            let cellRePresent = hotNewsTableView.dequeueReusableCell(withIdentifier: "noDataCell", for: indexPath) as! NoDataCell
            return cellRePresent
        }
        if indexPath.row == 0 {
            let cellRePresent = hotNewsTableView.dequeueReusableCell(withIdentifier: REPRECENT_IDENTIFIER_CELL, for: indexPath) as! RepresentSubChapCell
            cellRePresent.representImage.image = hotNewcategories[indexPath.row].image
            cellRePresent.nameTitleLabel.text = hotNewcategories[indexPath.row].name
            cellRePresent.ThoiSuButton.setTitle(hotNewcategories[indexPath.row].nameLevel, for: .normal)
            cellRePresent.timeButton.setTitle("21 phút trước", for: .normal)
        }else {
            let cell  = hotNewsTableView.dequeueReusableCell(withIdentifier: CHAP_MODULE_IDENTIFIER_CELL, for: indexPath) as! ChapModuleCell
            cell.chapImageView.image = hotNewcategories[indexPath.row].image
            cell.levelLabel.text = hotNewcategories[indexPath.row].nameLevel
            cell.titleChapLabel.text = hotNewcategories[indexPath.row].name
            cell.supperChapView.layer.cornerRadius = 5
            return cell
        }
        return UITableViewCell()

    }

    func goToDetailView(cate : CategoryObject) {
        let details = initViewController(MENU_PAGE_STORYBOARD, storyid: "ChapDetailsViewController") as! ChapDetailsViewController
        details.header = cate.name
        details.content = cate.content
        details.titlePage = "Tin Nóng"
        self.pushViewControllerInTab(ViewController: details)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let levelPost = hotNewcategories[indexPath.row].level
        let idLevel = (DataAccount.shared().userInfo?.level) ?? ""

        if levelPost == "" {
            goToDetailView(cate: hotNewcategories[indexPath.row])
            return
        }else {
            if DataAccount.shared().typeUser == .ThuyKieuUser {
                if idLevel != "" {
                    goToDetailView(cate: hotNewcategories[indexPath.row])
                    return
                }else {
                    showAlertCustom(self, title: "ThuyKieu", message: "Không thể truy cập , vui lòng nâng cấp account", buttonFirst: "OK", buttonSecond: "Cancel", completionHandleFirst: {
                        let vc = initViewController(REGISTER_STORYBOARD, storyid: "PaymentInfoViewController")
                        self.pushViewControllerInTab(ViewController: vc)
                    }, completionHandleSecond: {return})
                    return
                }
            }else {
                showAlertCustom(self, title: "ThuyKieu", message: "Không thể truy cập , vui lòng đăng kí tài khoản", buttonFirst: "OK", buttonSecond: "Cancel", completionHandleFirst: {
                    GET_APP_DELEGATE.setWelcomeToRootViewController()
                }, completionHandleSecond: {})
            }
        }


    }
}
