

import UIKit

class SucKhoeGioiTinhViewController: BaseMenuViewController {


    @IBOutlet weak var skhoeGtinhTableView: UITableView!

    var listSkhoeGtinhChap : [CategoryObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        CategoriesService.callGetCategorySubChanel(typeCate: .SKHOE_GTINH) { (dictionary) in
//            self.skhoeGtinhTableView.reloadData()
//        }
        skhoeGtinhTableView.reloadData()
            animateTableView()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension SucKhoeGioiTinhViewController {

    func setUpView() {
        initTableView()
//        skgtcategories = DataAccount.shared().categoriesSKhoeGioiTinh!
//        if skgtcategories.count == 10000 {
//            skhoeGtinhTableView.separatorStyle = .none
//            skhoeGtinhTableView.isScrollEnabled = false
//            skhoeGtinhTableView.allowsSelection = false
//        }
        listSkhoeGtinhChap = DataAccount.shared().categoriesSKhoeGioiTinh!
        if listSkhoeGtinhChap.count == 0 {
            skhoeGtinhTableView.separatorStyle = .none
            skhoeGtinhTableView.allowsSelection = false
        }
        skhoeGtinhTableView.tableFooterView = UIView()
        skhoeGtinhTableView.delegate = self
        skhoeGtinhTableView.dataSource = self
        skhoeGtinhTableView.backgroundColor = AppColors.backgroundRegisterColor
        skhoeGtinhTableView.registerNib(nibName: CHAP_MODULE_CELL_NIB, identifier: CHAP_MODULE_IDENTIFIER_CELL)
        skhoeGtinhTableView.registerNib(nibName: REPRECENT_SUB_CHAP_CELL_NIB, identifier: REPRECENT_IDENTIFIER_CELL)
        skhoeGtinhTableView.registerNib(nibName: "NoDataCell", identifier: "noDataCell")
        view.backgroundColor = AppColors.backgroundPageBar
        skhoeGtinhTableView.backgroundColor = AppColors.backgroundPageBar
    }

    func initTableView() {
        skhoeGtinhTableView.registerNib(nibName: CHAP_CELL_NIB, identifier: CHAP_IDENTIFIER_CELL)
        skhoeGtinhTableView.delegate = self
        skhoeGtinhTableView.dataSource = self
    }

    func animateTableView() {
        skhoeGtinhTableView.reloadData()
        let cells = skhoeGtinhTableView.visibleCells
        let tableHeight: CGFloat = skhoeGtinhTableView.bounds.size.height
        var index = 0

        for indexModule in cells {
            let cell: UITableViewCell = indexModule
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        for indexCell in cells {
            let cell: UITableViewCell = indexCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
}

extension SucKhoeGioiTinhViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if listSkhoeGtinhChap.count == 0 {
            return 1
        }
        return listSkhoeGtinhChap.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if listSkhoeGtinhChap.count == 0 {
            return self.view.frame.height
        }
        if listSkhoeGtinhChap.count == 1 {
            return 94
        }else if listSkhoeGtinhChap.count > 1  {
            if indexPath.row == 0 {
                return 180
            } else {
                return 94
            }
        }
        return 94
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if listSkhoeGtinhChap.count == 0 {
            let cellRePresent = skhoeGtinhTableView.dequeueReusableCell(withIdentifier: "noDataCell", for: indexPath) as! NoDataCell
            return cellRePresent
        }
        if listSkhoeGtinhChap.count == 1 {
            let cell  = skhoeGtinhTableView.dequeueReusableCell(withIdentifier: CHAP_MODULE_IDENTIFIER_CELL, for: indexPath) as! ChapModuleCell
            cell.chapImageView.image = listSkhoeGtinhChap[indexPath.row].image
            cell.levelLabel.text = listSkhoeGtinhChap[indexPath.row].nameLevel
            cell.titleChapLabel.text = listSkhoeGtinhChap[indexPath.row].name
            cell.supperChapView.layer.cornerRadius = 5
            return cell
        }else if listSkhoeGtinhChap.count > 1 {
            if indexPath.row == 0 {
                let cellRePresent = skhoeGtinhTableView.dequeueReusableCell(withIdentifier: REPRECENT_IDENTIFIER_CELL, for: indexPath) as! RepresentSubChapCell
                cellRePresent.representImage.image = listSkhoeGtinhChap[indexPath.row].image
                cellRePresent.nameTitleLabel.text = listSkhoeGtinhChap[indexPath.row].name
                cellRePresent.ThoiSuButton.setTitle(listSkhoeGtinhChap[indexPath.row].nameLevel, for: .normal)
                cellRePresent.timeButton.setTitle("21 phút trước", for: .normal)
            }else {
                let cell  = skhoeGtinhTableView.dequeueReusableCell(withIdentifier: CHAP_MODULE_IDENTIFIER_CELL, for: indexPath) as! ChapModuleCell
                cell.chapImageView.image = listSkhoeGtinhChap[indexPath.row].image
                cell.levelLabel.text = listSkhoeGtinhChap[indexPath.row].nameLevel
                cell.titleChapLabel.text = listSkhoeGtinhChap[indexPath.row].name
                cell.supperChapView.layer.cornerRadius = 5
                return cell
            }
        }
        return UITableViewCell()

    }

    func goToDetailView(cate : CategoryObject) {
        let details = initViewController(MENU_PAGE_STORYBOARD, storyid: "ChapDetailsViewController") as! ChapDetailsViewController
        details.header = cate.name
        details.content = cate.content
        details.titlePage = "Sức Khoẻ Giới Tính"
        self.pushViewControllerInTab(ViewController: details)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let levelPost = listSkhoeGtinhChap[indexPath.row].level
        let idLevel = (DataAccount.shared().userInfo?.level) ?? ""

        if levelPost == "" {
            goToDetailView(cate: listSkhoeGtinhChap[indexPath.row])
            return
        }else {
            if DataAccount.shared().typeUser == .ThuyKieuUser {
                if idLevel != "" {
                    goToDetailView(cate: listSkhoeGtinhChap[indexPath.row])
                    return
                }else {
                    showAlertCustom(self, title: "ThuyKieu", message: "Không thể truy cập , vui lòng nâng cấp account", buttonFirst: "OK", buttonSecond: "Cancel", completionHandleFirst: {
                        let vc = initViewController(REGISTER_STORYBOARD, storyid: "PaymentInfoViewController")
                        self.pushViewControllerInTab(ViewController: vc)
                    }, completionHandleSecond: {return})
                    return
                }
            }else {
                showAlertCustom(self, title: "ThuyKieu", message: "Không thể truy cập , vui lòng đăng kí tài khoản", buttonFirst: "OK", buttonSecond: "Cancel", completionHandleFirst: {
                    GET_APP_DELEGATE.setWelcomeToRootViewController()
                }, completionHandleSecond: {})
            }
        }
    }

}


