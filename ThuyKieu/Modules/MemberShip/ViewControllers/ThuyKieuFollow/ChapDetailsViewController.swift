//
//  ChapDetailsViewController.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 5/23/17.
//  Copyright © 2017 vn. All rights reserved.
//ChapCellDetailCell

import UIKit

class ChapDetailsViewController: BaseViewController {

    var header  : String = ""
    var content : String = ""
    var titlePage : String = ""
    @IBOutlet weak var detailWebView: UIWebView!


    override func viewDidLoad() {
        super.viewDidLoad()
        initCustomNavigationView()
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ChapDetailsViewController {

    func setUpView() {
        customizeNavigationView?.tittleLabel.text = titlePage
        var textContent  = self.content.replace("width", replacement: "width = 100% width, height = width")
        textContent = textContent.replace("<strong>", replacement: "<strong><br>")
        textContent = textContent.replace("<img", replacement: "<br><img")
        textContent = textContent.replace("</strong>", replacement: "</strong><br>")
        textContent = textContent.replace("</em>", replacement: "</em><br>")

        detailWebView.scalesPageToFit = true
        detailWebView.contentMode = UIViewContentMode.center
            detailWebView.loadHTMLString("<html><head><style type='text/css'>html,body {margin: 0;padding: 0;width: 100%;height: 100%; font-style: arial - regular; font-size: 70px;}html {display: table;}body {display: table-cell;vertical-align: middle;padding: 20px;text-align: left;-webkit-text-size-adjust: none;} img{width= 100%}</style></head><body><h2 style = text-align: center;>\(self.header)</h2><p>\(textContent)</p></body></html>​", baseURL: nil)
    }


}
