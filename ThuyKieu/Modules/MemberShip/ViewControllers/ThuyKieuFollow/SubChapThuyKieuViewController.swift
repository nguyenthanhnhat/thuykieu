
import UIKit

class SubChapThuyKieuViewController: BaseViewController {

    var subChapTableView = UITableView()

    var titlePage = ""

    var listThuyKieuChap : [CategoryObject] = []
    var idTap = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        CategoriesService.callGetCategorySubChanel(typeCate: .SUB_THUYKIEU) { (dictionary) in
//            self.subChapTableView.reloadData()
//        }
        subChapTableView.reloadData()
        animateTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension SubChapThuyKieuViewController {

    func setUpView() {
        initTableView()
        initCustomNavigationView()
        customizeNavigationView?.tittleLabel.text = titlePage
        subChapTableView.delegate = self
        subChapTableView.dataSource = self
        listThuyKieuChap = DataAccount.shared().categoriesSubThuyKieu!
        if listThuyKieuChap.count == 0 {
            subChapTableView.separatorStyle = .none
            subChapTableView.allowsSelection = false
        }
        subChapTableView.tableFooterView = UIView()
        subChapTableView.backgroundColor = AppColors.backgroundRegisterColor
        subChapTableView.registerNib(nibName: CHAP_MODULE_CELL_NIB, identifier: CHAP_MODULE_IDENTIFIER_CELL)
        subChapTableView.registerNib(nibName: REPRECENT_SUB_CHAP_CELL_NIB, identifier: REPRECENT_IDENTIFIER_CELL)
        subChapTableView.registerNib(nibName: "NoDataCell", identifier: "noDataCell")
//        }

        view.backgroundColor = AppColors.backgroundPageBar
        subChapTableView.backgroundColor = AppColors.backgroundPageBar
    }

    func initTableView() {
        let heightNavi : CGFloat = 64
        subChapTableView.registerNib(nibName: CHAP_CELL_NIB, identifier: CHAP_IDENTIFIER_CELL)
        subChapTableView.frame = CGRect(x: 0, y: heightNavi, width: view.frame.width, height: view.frame.height - heightNavi)
        subChapTableView.delegate = self
        subChapTableView.dataSource = self
        view.addSubview(subChapTableView)
    }

    func animateTableView() {
        subChapTableView.reloadData()
        let cells = subChapTableView.visibleCells
        let tableHeight: CGFloat = subChapTableView.bounds.size.height
        var index = 0

        for indexModule in cells {
            let cell: UITableViewCell = indexModule
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        for indexCell in cells {
            let cell: UITableViewCell = indexCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }

    func goToDetailView(cate : CategoryObject) {
        let details = initViewController(MENU_PAGE_STORYBOARD, storyid: "ChapDetailsViewController") as! ChapDetailsViewController
        details.header = cate.name
        details.content = cate.content
        details.titlePage = cate.name
        self.pushViewControllerInTab(ViewController: details)
    }
}

extension SubChapThuyKieuViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if listThuyKieuChap.count == 0  {
            return 1
        }
        return listThuyKieuChap.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if listThuyKieuChap.count == 0  {
            return self.view.frame.height
        }
        if listThuyKieuChap.count == 1  {
            return 94
        }
        if indexPath.row == 0 {
            return 180
        }
        return 94
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if listThuyKieuChap.count == 0 {
            let cellRePresent = subChapTableView.dequeueReusableCell(withIdentifier: "noDataCell", for: indexPath) as! NoDataCell
            return cellRePresent
        }
        if listThuyKieuChap.count == 1  {
            let cell  = subChapTableView.dequeueReusableCell(withIdentifier: CHAP_MODULE_IDENTIFIER_CELL, for: indexPath) as! ChapModuleCell
            cell.chapImageView.image = listThuyKieuChap[indexPath.row].image
            cell.levelLabel.text = listThuyKieuChap[indexPath.row].nameLevel
            cell.titleChapLabel.text = listThuyKieuChap[indexPath.row].name
            cell.supperChapView.layer.cornerRadius = 5
            return cell
        }
        if indexPath.row == 0 {
            let cellRePresent = subChapTableView.dequeueReusableCell(withIdentifier: REPRECENT_IDENTIFIER_CELL, for: indexPath) as! RepresentSubChapCell
            cellRePresent.representImage.image = listThuyKieuChap[indexPath.row].image
            cellRePresent.nameTitleLabel.text = listThuyKieuChap[indexPath.row].name
            cellRePresent.ThoiSuButton.setTitle(listThuyKieuChap[indexPath.row].nameLevel, for: .normal)
            cellRePresent.timeButton.setTitle("21 phút trước", for: .normal)
        }else {
            let cell  = subChapTableView.dequeueReusableCell(withIdentifier: CHAP_MODULE_IDENTIFIER_CELL, for: indexPath) as! ChapModuleCell
            cell.chapImageView.image = listThuyKieuChap[indexPath.row].image
            cell.levelLabel.text = listThuyKieuChap[indexPath.row].nameLevel
            cell.titleChapLabel.text = listThuyKieuChap[indexPath.row].name
            cell.supperChapView.layer.cornerRadius = 5
            return cell
        }
        return UITableViewCell()
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let levelPost = listThuyKieuChap[indexPath.row].level
        let idLevel = (DataAccount.shared().userInfo?.level) ?? "0"
        if levelPost == "" {
            goToDetailView(cate: listThuyKieuChap[indexPath.row])
            return
        }else {
            if DataAccount.shared().typeUser == .ThuyKieuUser {
                if idLevel != "" {
                    goToDetailView(cate: listThuyKieuChap[indexPath.row])
                    return
                }else {
                    showAlertCustom(self, title: "ThuyKieu", message: "Không thể truy cập , vui lòng nâng cấp account", buttonFirst: "OK", buttonSecond: "Cancel", completionHandleFirst: {
                        let vc = initViewController(REGISTER_STORYBOARD, storyid: "PaymentInfoViewController")
                        self.pushViewControllerInTab(ViewController: vc)
                    }, completionHandleSecond: {return})
                    return
                }
            }else {
                showAlertCustom(self, title: "ThuyKieu", message: "Không thể truy cập , vui lòng đăng kí tài khoản", buttonFirst: "OK", buttonSecond: "Cancel", completionHandleFirst: {
                    GET_APP_DELEGATE.setWelcomeToRootViewController()
                }, completionHandleSecond: {})
            }
        }
    }


}

