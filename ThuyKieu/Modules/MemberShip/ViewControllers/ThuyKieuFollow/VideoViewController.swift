//
//  VideoViewController.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 6/4/17.
//  Copyright © 2017 vn. All rights reserved.
//


import UIKit

class VideoViewController: BaseMenuViewController {


    @IBOutlet weak var videoTableView: UITableView!

    var videoChaps : [CategoryObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        CategoriesService.callGetCategorySubChanel(typeCate: .VIDEO) { (dictionary) in
//            self.videoTableView.reloadData()
//        }
        videoTableView.reloadData()
        animateTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension VideoViewController {

    func setUpView() {
        initTableView()
        videoTableView.delegate = self
        videoTableView.dataSource = self
        videoTableView.tableFooterView = UIView()
        videoChaps = DataAccount.shared().categoriesVideo!
        if videoChaps.count == 0 {
            videoTableView.separatorStyle = .none
            videoTableView.allowsSelection = false
        }
        videoTableView.backgroundColor = AppColors.backgroundRegisterColor
        videoTableView.registerNib(nibName: CHAP_MODULE_CELL_NIB, identifier: CHAP_MODULE_IDENTIFIER_CELL)
        videoTableView.registerNib(nibName: REPRECENT_SUB_CHAP_CELL_NIB, identifier: REPRECENT_IDENTIFIER_CELL)
        videoTableView.registerNib(nibName: "NoDataCell", identifier: "noDataCell")
        view.backgroundColor = AppColors.backgroundPageBar
        videoTableView.backgroundColor = AppColors.backgroundPageBar
    }

    func initTableView() {
        videoTableView.registerNib(nibName: CHAP_CELL_NIB, identifier: CHAP_IDENTIFIER_CELL)
        videoTableView.delegate = self
        videoTableView.dataSource = self
    }

    func animateTableView() {
        videoTableView.reloadData()
        let cells = videoTableView.visibleCells
        let tableHeight: CGFloat = videoTableView.bounds.size.height
        var index = 0

        for indexModule in cells {
            let cell: UITableViewCell = indexModule
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        for indexCell in cells {
            let cell: UITableViewCell = indexCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
}

extension VideoViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if videoChaps.count == 0 {
            return 1
        }
        return videoChaps.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if videoChaps.count == 0 {
            return self.view.frame.height
        }
        if videoChaps.count == 1 {
            return 94
        }
        if indexPath.row == 0 {
            return 180
        }
        return 94
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if videoChaps.count == 0 {
            let cellRePresent = videoTableView.dequeueReusableCell(withIdentifier: "noDataCell", for: indexPath) as! NoDataCell
            return cellRePresent
        }
        if videoChaps.count == 1 {
            let cell  = videoTableView.dequeueReusableCell(withIdentifier: CHAP_MODULE_IDENTIFIER_CELL, for: indexPath) as! ChapModuleCell
            cell.chapImageView.image = videoChaps[indexPath.row].image
            cell.levelLabel.text = videoChaps[indexPath.row].nameLevel
            cell.titleChapLabel.text = videoChaps[indexPath.row].name
            cell.supperChapView.layer.cornerRadius = 5
            return cell
        }else {
            if indexPath.row == 0 {
                let cellRePresent = videoTableView.dequeueReusableCell(withIdentifier: REPRECENT_IDENTIFIER_CELL, for: indexPath) as! RepresentSubChapCell
                cellRePresent.representImage.image = videoChaps[indexPath.row].image
                cellRePresent.nameTitleLabel.text = videoChaps[indexPath.row].name
                cellRePresent.ThoiSuButton.setTitle(videoChaps[indexPath.row].nameLevel, for: .normal)
                cellRePresent.timeButton.setTitle("21 phút trước", for: .normal)
            }else {
                let cell  = videoTableView.dequeueReusableCell(withIdentifier: CHAP_MODULE_IDENTIFIER_CELL, for: indexPath) as! ChapModuleCell
                cell.chapImageView.image = videoChaps[indexPath.row].image
                cell.levelLabel.text = videoChaps[indexPath.row].nameLevel
                cell.titleChapLabel.text = videoChaps[indexPath.row].name
                cell.supperChapView.layer.cornerRadius = 5
                return cell
            }
        }
        return UITableViewCell()
    }

    func goToDetailView(cate : CategoryObject) {
        let details = initViewController(MENU_PAGE_STORYBOARD, storyid: "ChapDetailsViewController") as! ChapDetailsViewController
        details.header = cate.name
        details.content = cate.content
        details.titlePage = "Video"
        self.pushViewControllerInTab(ViewController: details)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let levelPost = videoChaps[indexPath.row].level
        let idLevel = (DataAccount.shared().userInfo?.level) ?? "0"

        if levelPost == "" {
            goToDetailView(cate: videoChaps[indexPath.row])
            return
        }else {
            if DataAccount.shared().typeUser == .ThuyKieuUser {
                if idLevel != "" {
                    goToDetailView(cate: videoChaps[indexPath.row])
                    return
                }else {
                    showAlertCustom(self, title: "ThuyKieu", message: "Không thể truy cập , vui lòng nâng cấp account", buttonFirst: "OK", buttonSecond: "Cancel", completionHandleFirst: {
                        let vc = initViewController(REGISTER_STORYBOARD, storyid: "PaymentInfoViewController")
                        self.pushViewControllerInTab(ViewController: vc)
                    }, completionHandleSecond: {return})
                    return
                }
            }else {
                showAlertCustom(self, title: "ThuyKieu", message: "Không thể truy cập , vui lòng đăng kí tài khoản", buttonFirst: "OK", buttonSecond: "Cancel", completionHandleFirst: {
                    GET_APP_DELEGATE.setWelcomeToRootViewController()
                }, completionHandleSecond: {})
            }
        }


    }
}
