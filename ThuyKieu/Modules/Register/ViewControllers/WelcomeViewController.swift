

import UIKit

class WelcomeViewController: BaseRegisterViewController {

    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var signInLaterButton: UIButton!
    @IBOutlet weak var createAccoutLabel: UILabel!
    @IBOutlet weak var LoginLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func SignInAction(_ sender: Any) {
        let vc = initViewController(REGISTER_STORYBOARD, storyid: SIGN_IN_VIEW_CONTROLER)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        let vc = initViewController(REGISTER_STORYBOARD, storyid: SIGN_UP_VIEW_CONTROLER)
        navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func sigInLaterButton(_ sender: Any) {
        DataAccount.shared().typeUser = .GuestUser
        GET_APP_DELEGATE.setSlideToRootViewController()
    }
}


extension WelcomeViewController {
    func setUpView() {
        signInButton = boderButton(button: signInButton)
        signUpButton = boderButton(button: signUpButton)
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "thuykieu_welcome")
        self.view.insertSubview(backgroundImage, at: 0)
    }

    func boderButton(button : UIButton) -> UIButton{
        button.layer.cornerRadius = 3
        return button
    }

}
