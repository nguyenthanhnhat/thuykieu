//
//  ForgotPasswordViewController.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 5/12/17.
//  Copyright © 2017 vn. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseRegisterViewController {
    @IBOutlet weak var emailTextField   : UITextField!
    @IBOutlet weak var submitButton     : UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func didClickResetPass(_ sender: UIButton) {
        callService()
    }

    func callService() {
        let email = emailTextField.text?.trim()
        if email == "" || !isValidEmail(email!){
            showAlertCustomOneButton(self, title: DataConstant.appName, message: "Mail không nhập trống hoặc sai format .", buttonFirst: "OK") {}
            return
        }
        GET_APP_DELEGATE.showActivityWithLabel(string: "Loading...")
        RegisterService.callCheckAccountExistingService(insecure: DataConstant.insecure, email: email!) { (dictionary) in
            GET_APP_DELEGATE.hideActivity()
            if dictionary != nil {
                log("dic \(String(describing: dictionary))")
                let dic = JSON(dictionary!).object
                let data = dic?[DATA_RESPOND]?.object
                let status = data?[STATUS]?.string

                let message = data?["msg"]?.string!
                if status == "ok" && message != nil {
                    showAlertCustomOneButton(self, title: DataConstant.appName, message: message!, buttonFirst: "OK") {
                        self.popToViewController(SignInViewController.self)
                    }
                    return
                }
            }
            showAlertCustomOneButton(self, title: DataConstant.appName, message: "An error occur. please try again", buttonFirst: "OK") {}
        }

    }
}

extension ForgotPasswordViewController {

    func setUpView() {
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "thuykieu_welcome")
        self.view.insertSubview(backgroundImage, at: 0)
        navigationController?.isNavigationBarHidden = false

        submitButton = setBoderButton(button: submitButton, color: UIColor.white.cgColor)
    }
}
