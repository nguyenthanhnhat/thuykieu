
import UIKit

class BaseMainViewController: BaseViewController, RegisterNavigationViewDelegate {
    
    var registerNavigationView : RegisterNavigationView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpRegisterBaseView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension BaseMainViewController{
    func setUpRegisterBaseView() {
        mainNavigationView?.isHidden = true
        initRegisterNavigationView()
        view.backgroundColor = AppColors.backgroundRegisterColor
    }

    func initRegisterNavigationView(){
        registerNavigationView = RegisterNavigationView.loadFromNibNamed("RegisterNavigationView") as? RegisterNavigationView
        registerNavigationView?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 64)
        registerNavigationView?.delegate = self
        registerNavigationView?.backgroundColor = AppColors.backgroundRegisterColor
        registerNavigationView?.openTimeLabel.isHidden = true
        registerNavigationView?.tittleLabel.isHidden = true
        view.addSubview(registerNavigationView!)

    }
//
//    override func didClickBack() {
//        clickBack()
//    }
}
