

import UIKit

class SignInViewController: BaseRegisterViewController {

    @IBOutlet weak var ThuyKieuLabel: UILabel!
    @IBOutlet weak var signInLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var forgotPassButton: UIButton!
    @IBOutlet weak var createAccountButton: UIButton!



    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.isNavigationBarHidden = true
    }

    @IBAction func emailEditChanged(_ sender: UITextField) {
        checkValidateLogin()
    }
    
    @IBAction func passEditChanged(_ sender: UITextField) {
        checkValidateLogin()
    }

    @IBAction func toToSignIn(_ sender: Any) {
        goToSignInScreen()
    }

    @IBAction func goToForgotPass(_ sender: Any) {
        goToForgotScreeen()
    }


    @IBAction func signUpAction(_ sender: UIButton) {
        createNewAccount()
    }

}

extension SignInViewController {

    func setUpView() {
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "thuykieu_welcome")
        self.view.insertSubview(backgroundImage, at: 0)

        emailTextField.delegate = self
        passwordTextField.delegate = self
        emailTextField.adjustsFontSizeToFitWidth = true
        signInButton.isEnabled = true

        signInButton = setBoderButton(button: signInButton, color: UIColor.white.cgColor)
        createAccountButton = setBoderButton(button: createAccountButton, color: UIColor.darkGray.cgColor)
    }

    func goToSignInScreen() {
        let email = emailTextField.text?.trim() ?? ""
        let password = passwordTextField.text?.trim() ?? ""
        let insecure = DataConstant.insecure
        if email == "" || password == "" {
            showAlertCustomOneButton(self, title: "ThuyKieu", message: "Yêu cầu nhập đầy đủ thông tin.", buttonFirst: "OK") {}
            return
        }
        GET_APP_DELEGATE.showActivityWithLabel(string: "Loading...")
        RegisterService.callLoginService(insecure : insecure ,email: email, password: password) { (dictionary) in
            GET_APP_DELEGATE.hideActivity()
            if dictionary != nil {
                log("dic \(String(describing: dictionary))")
                let dic = JSON(dictionary!).object
                let data = dic?[DATA_RESPOND]?.object
                let status = data?[STATUS]?.string
                let message = data?["error"]?.string ?? "An error occur. please try again"
                if status == "ok" {
                    DataAccount.shared().typeUser = .ThuyKieuUser
                    DataAccount.shared().userInfo = UserInfoObject()
                    DataAccount.shared().userInfo?.email = JSON(data?["user"]?["email"] as Any).string!
                    DataAccount.shared().userInfo?.id =  String(JSON(data?["user"]?["id"]).integer!)
                    if let levelArray = data?["user"]?["level"].array, levelArray.count > 0 {
                        DataAccount.shared().userInfo?.level = ((data?["user"]?["level"].array)![0]["level_id"]).string!
                    }else {
                        DataAccount.shared().userInfo?.level = ""
                    }
                    GET_APP_DELEGATE.setSlideToRootViewController()
                    return
                }
                showAlertCustomOneButton(self, title: DataConstant.appName, message: message, buttonFirst: "OK") {}
                return
            }
            showAlertCustomOneButton(self, title: DataConstant.appName, message: "An error occur. please try again", buttonFirst: "OK") {}
        }
    }


    func goToForgotScreeen() {
        closeKeyBoard()
        let vc = initViewController(REGISTER_STORYBOARD, storyid: FORGOT_PASSWORD_VIEW_CONTROLER)
        navigationController?.pushViewController(vc, animated: true)
    }

    func checkValidateLogin() {
        let isEmail = isValidEmail(emailTextField.text!.trim())
        let isPass = isCheckLength(passwordTextField.text!.trim())
        signInButton.isEnabled = (isEmail && isPass) ? true : false
    }

    func createNewAccount() {
        _  = self.navigationController?.popToRootViewController(animated: true)
    }
}

extension SignInViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 0 {
            passwordTextField.becomeFirstResponder()
        }else {
            if signInButton.isEnabled {
                closeKeyBoard()
                goToSignInScreen()
            }
        }
        return false
    }

}
