
import UIKit

class BaseRegisterViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpRegisterBaseView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension BaseRegisterViewController {
    func customRegisterNavigationViewSystem(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }

    func setUpRegisterBaseView() {
        customRegisterNavigationViewSystem()
        addEventTouchInView()
    }

    func addEventTouchInView () -> Void {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.touchInView(_:)))
        self.view.addGestureRecognizer(gesture)
    }

    func closeKeyBoard()  {
        self.view.endEditing(true)
    }
}

extension BaseRegisterViewController : UIGestureRecognizerDelegate {
    func touchInView(_ sender:UITapGestureRecognizer) {
        closeKeyBoard()
    }
}
