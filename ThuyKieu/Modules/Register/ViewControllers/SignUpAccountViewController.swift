

import UIKit



class SignUpAccountViewController: BaseRegisterViewController {

    @IBOutlet weak var thuyKieuLabel: UILabel!
    @IBOutlet weak var createAccountLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var vangLaiButton: UIButton!
    @IBOutlet weak var vangLaiLabel: UILabel!
    @IBOutlet weak var thuongButton: UIButton!
    @IBOutlet weak var thuongLabel: UILabel!
    @IBOutlet weak var vipButton: UIButton!
    @IBOutlet weak var vipLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var createAccountButton: UIButton!


    var isShowNewPass = false {
        didSet {
            passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
        }
    }

    var isCheckedVangLai = false {
        didSet {
            vangLaiButton.setImage(UIImage(named : isCheckedVangLai ? "checked_cycle_icon" : "unCheck_cycle_icon"), for: .normal)
            thuongButton.setImage(UIImage(named : "unCheck_cycle_icon"), for: .normal)
            vipButton.setImage(UIImage(named : "unCheck_cycle_icon"), for: .normal)
        }

    }

    var isCheckedNomal = false {
        didSet {
            thuongButton.setImage(UIImage(named : isCheckedNomal ? "checked_cycle_icon" : "unCheck_cycle_icon"), for: .normal)
            vangLaiButton.setImage(UIImage(named : "unCheck_cycle_icon"), for: .normal)
            vipButton.setImage(UIImage(named : "unCheck_cycle_icon"), for: .normal)
        }
    }

    var isCheckedVip = false {
        didSet {
            vipButton.setImage(UIImage(named : isCheckedVip ? "checked_cycle_icon" : "unCheck_cycle_icon"), for: .normal)
            vangLaiButton.setImage(UIImage(named : "unCheck_cycle_icon"), for: .normal)
            thuongButton.setImage(UIImage(named : "unCheck_cycle_icon"), for: .normal)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func registeraction(_ sender: UIButton) {
        clickCreateAccount()
    }

    @IBAction func loginAction(_ sender: UIButton) {
        clickLogin()
    }

    @IBAction func vangLaiAction(_ sender: UIButton) {
        isCheckedVangLai = !isCheckedVangLai
    }

    @IBAction func ThuongAction(_ sender: UIButton) {
        isCheckedNomal = !isCheckedNomal
    }

    @IBAction func vipAction(_ sender: UIButton) {
        isCheckedVip = !isCheckedVip
    }

    @IBAction func emailEditChange(_ sender: UITextField) {
        validateSignUp()
    }

    @IBAction func passwordEditChanged(_ sender: UITextField) {
        validateSignUp()
    }

    @IBAction func fullNameEditChanged(_ sender: UITextField) {
        validateSignUp()
    }

    @IBAction func deleteEmailAction(_ sender: UIButton) {
        emailTextField.text = ""
    }

    @IBAction func showPasswordAction(_ sender: UIButton) {
        isShowNewPass = !isShowNewPass

    }
}

extension SignUpAccountViewController {
    func setUpView() {
        navigationController?.isNavigationBarHidden = false
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "thuykieu_welcome")
        self.view.insertSubview(backgroundImage, at: 0)
        loginButton = setBoderButton(button: loginButton, color: UIColor.darkText.cgColor)
    }

    func callLoginService() {
        let email = emailTextField.text?.trim() ?? ""
        let password = passwordTextField.text?.trim() ?? ""

        if email == "" || password == "" {
            return
        }
        GET_APP_DELEGATE.showActivityWithLabel(string: "Đăng kí thành công...")
        RegisterService.callLoginService(insecure: DataConstant.insecure, email: email, password: password) { (dictionary) in
            GET_APP_DELEGATE.hideActivity()
            if dictionary != nil {
                log("dic \(String(describing: dictionary))")
                let dic = JSON(dictionary!).object
                let data = dic?[DATA_RESPOND]?.object
                let status = data?[STATUS]?.string
                let message = data?["error"]?.string ?? "An error occur. please try again"
                if status == "ok" {
                    DataAccount.shared().userInfo = UserInfoObject()
                    DataAccount.shared().userInfo?.email = JSON(data?["user"]?["email"] as Any).string!
                    DataAccount.shared().userInfo?.id =  String(JSON(data?["user"]?["id"]).integer!)
                    if let levelArray = data?["user"]?["level"].array, levelArray.count > 0 {
                        DataAccount.shared().userInfo?.level = ((data?["user"]?["level"].array)![0]["level_id"]).string!
                    }else {
                        DataAccount.shared().userInfo?.level = ""
                    }
                    GET_APP_DELEGATE.setSlideToRootViewController()
                    return
                }
                showAlertCustomOneButton(self, title: DataConstant.appName, message: message, buttonFirst: "OK") {}
                return
            }
            showAlertCustomOneButton(self, title: DataConstant.appName, message: "An error occur. please try again", buttonFirst: "OK") {}
        }

    }

    func clickCreateAccount() {
        closeKeyBoard()
        if emailTextField.text?.trim() == ""  || fullNameTextField.text?.trim() == "" || passwordTextField.text?.trim() == "" {
            showAlertCustomOneButton(self, title: "ThuyKieu", message: "Yêu cầu nhập đầy đủ thông tin.", buttonFirst: "OK") {}
            return
        }

        let userObject = UserInfoObject()
        userObject.email = emailTextField.text?.trim() ?? ""
        userObject.username = getUsernameFromEmail(email: userObject.email)
        userObject.fullname = userObject.username
        userObject.level = ""
        userObject.password = passwordTextField.text?.trim() ?? ""

        GET_APP_DELEGATE.showActivityWithLabel(string: "Loading...")
        RegisterService.callRegisterService(userInfoObject: userObject){ (dictionary) in
            GET_APP_DELEGATE.hideActivity()
            if dictionary != nil {
                DataAccount.shared().typeUser = .ThuyKieuUser
                log("dic \(String(describing: dictionary))")
                let dic = JSON(dictionary!).object
                let data = dic?[DATA_RESPOND]?.object
                let status = data?[STATUS]?.string
                let message = data?["error"]?.string ?? "An error occur. please try again"
                if status == "ok" {
                        self.callLoginService()
                    return
                }
                showAlertCustomOneButton(self, title: DataConstant.appName, message: message, buttonFirst: "OK") {}
                return
            }
            showAlertCustomOneButton(self, title: DataConstant.appName, message: "An error occur. please try again", buttonFirst: "OK") {}
        }
    }

    func validateSignUp() {
        let email = emailTextField.text?.trim()
        let password = passwordTextField.text?.trim()
        let fullName = fullNameTextField.text?.trim()
        let isValid = isValidEmail(email!)
        let isValidPass = isCheckLength(password!)
        let isValidFullName = isCheckLength(fullName!)
//        createAccountButton.isEnabled = (isValid && isValidPass && isValidFullName)
        createAccountButton.isEnabled =  true
    }

    func clickLogin() {
        navigationController?.popToRootViewController(animated: true)
    }

}
