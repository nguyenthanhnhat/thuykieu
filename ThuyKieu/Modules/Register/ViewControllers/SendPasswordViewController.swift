
import UIKit

class SendPasswordViewController: BaseRegisterViewController {

    @IBOutlet weak var titleFormLabel: UILabel!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var newPassTextField: UITextField!
    @IBOutlet weak var rePassTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var requirePassLabel: UILabel!
    @IBOutlet weak var showCodeButton: UIButton!
    @IBOutlet weak var showNewPassButton: UIButton!
    @IBOutlet weak var showConfirmPassButton: UIButton!
    @IBOutlet weak var validateCodeButton: UIButton!
    @IBOutlet weak var validateNewPassButton: UIButton!
    @IBOutlet weak var validateConfirmPassButton: UIButton!

    var isShowNewPass = false {
        didSet {
            newPassTextField.isSecureTextEntry = !newPassTextField.isSecureTextEntry
            showNewPassButton.setImage(UIImage(named : isShowNewPass ? "eye_icon" : "eye_hidden_icon"), for: .normal)
        }
    }

    var isShowConfirmPass = false {
        didSet {
            rePassTextField.isSecureTextEntry = !rePassTextField.isSecureTextEntry
            showConfirmPassButton.setImage(UIImage(named : isShowConfirmPass ? "eye_icon" : "eye_hidden_icon"), for: .normal)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    @IBAction func didClickSubmit(_ sender: Any) {
        callServiceUpdatePass()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func codeEditChange(_ sender: Any) {
        validateForm()
    }

    @IBAction func newPassEditChange(_ sender: Any) {
        validateForm()
    }

    @IBAction func rePassEditChange(_ sender: Any) {
        validateForm()
    }

    @IBAction func didClickShowNewPassword(_ sender: Any) {
        isShowNewPass = !isShowNewPass
    }

    @IBAction func didClickShowConfirmPassword(_ sender: Any) {
        isShowConfirmPass = !isShowConfirmPass
    }

}

extension SendPasswordViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        }else {
            textField.resignFirstResponder()
            if submitButton.isEnabled {
                callServiceUpdatePass()
            }
        }
        return false
    }
}

extension SendPasswordViewController{
    func setUpView() {
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "thuykieu_welcome")
        self.view.insertSubview(backgroundImage, at: 0)

        codeTextField.delegate = self
        newPassTextField.delegate = self
        rePassTextField.delegate = self
        submitButton.isEnabled = false
        showCodeButton.isHidden = true
    }

    func callServiceUpdatePass() {
        self.closeKeyBoard()
        print("didClick Submit")
    }

    func validateForm() {
        let isValidCode = isCheckLength(codeTextField.text!.trim())
        let isValidPass = isCheckLength(newPassTextField.text!.trim())
        let isValidConfirmPass = isCheckLength(rePassTextField.text!.trim()) && (newPassTextField.text!.trim() == rePassTextField.text!.trim())

        validateCodeButton.setImage(UIImage(named: isValidCode ? "checkedIcon" : ""), for: .normal)
        validateNewPassButton.setImage(UIImage(named: isValidPass ? "checkedIcon" : ""), for: .normal)
        validateConfirmPassButton.setImage(UIImage(named: isValidConfirmPass ? "checkedIcon" : ""), for: .normal)

        let isValidForm : Bool = isValidCode && isValidPass && isValidConfirmPass
        submitButton.isEnabled = isValidForm
    }
}

