//
//  PaymentInfoViewController.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 6/16/17.
//  Copyright © 2017 vn. All rights reserved.
//

import UIKit

class PaymentInfoViewController: BaseViewController {
    @IBOutlet weak var paymentLogoImageView: UIImageView!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var nameOnCardTextField: UITextField!
    @IBOutlet weak var cardNumberTextField: UITextField!
    @IBOutlet weak var expiryTextField: UITextField!
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var sendingButton: UIButton!

    var userObject : UserInfoObject = UserInfoObject()

    override func viewDidLoad() {
        super.viewDidLoad()
        initCustomNavigationView()
        sendingButton = setBoderButton(button: sendingButton, color: UIColor.white.cgColor)
        amountTextField.text = MoneyLevel.LEVEL_ONE
        amountTextField.isUserInteractionEnabled = false
        amountTextField.alpha = 0.6

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    @IBAction func didClickSending(_ sender: UIButton) {
        let expiry = expiryTextField.text?.trim()
        let isAmount = amountTextField.text?.trim() != ""
        let isNameOnCard = nameOnCardTextField.text?.trim() != ""
        let isCardNumber = cardNumberTextField.text?.trim() != ""
        let isExpire = expiry != ""
        let isZipCode = zipCodeTextField.text?.trim() != ""
        if isAmount && isNameOnCard && isCardNumber && isExpire && isZipCode {
            if (expiry?.characters.count)! != 5 {
                showAlertCustomOneButton(self, title: "ThuyKieu", message: "Epiry date sai mm/yy", buttonFirst: "OK", completionHandle: {
                    return
                })
            }else {
                let index = expiry?.index((expiry?.startIndex)!, offsetBy: 2)
                let month : String = (expiry?.substring(to: index!))!
                let index1 = expiry?.index((expiry?.startIndex)!, offsetBy: 3)
                let year : String = (expiry?.substring(from: index1!))!

                let numberCard = cardNumberTextField.text?.trim() ?? ""
                let cvv = zipCodeTextField.text?.trim() ?? ""
                let amount = amountTextField.text?.trim() ?? ""
                GET_APP_DELEGATE.showActivityWithLabel(string: "Loading...")
                CategoriesService.chartPaymentUpdateLevel(numberCard: numberCard, month: month, year: year, cvv: cvv, amount: amount, completionHandler: { (dictionary) in
                    GET_APP_DELEGATE.hideActivity()
                    print("fff \(dictionary)")
                    if let dic = dictionary {
                        let diction = JSON(dic).object
                        let data = diction?["data"]?.object
                        let status = data?["status"]?.string
                        let result = data?["result"]?.string
                        if status == "ok" && result != nil {
                            GET_APP_DELEGATE.showActivityWithLabel(string: "Updating level...")
                            CategoriesService.updateLevel(completionHandler: { (dictionaryUpdate) in
                                GET_APP_DELEGATE.hideActivity()
                                print("update = \(dictionaryUpdate)")
                                if let dicUpdate = dictionaryUpdate {
                                    let dictionUpdate = JSON(dicUpdate).object
                                    let dataUpdate = dictionUpdate?["data"]?.object
                                    let statusUpdate = dataUpdate?["status"]?.string
                                    let resultUpdate = dataUpdate?["result"]?.bool
                                    if statusUpdate == "ok" && resultUpdate != nil {
                                        showAlertCustomOneButton(self, title: "ThuyKieu", message: "Update level Success please login again", buttonFirst: "OK", completionHandle: {
                                            GET_APP_DELEGATE.setWelcomeToRootViewController()
                                            return
                                        })
                                        return
                                    }else {
                                        showAlertCustomOneButton(self, title: "ThuyKieu", message: "Update level failed", buttonFirst: "OK", completionHandle: {})
                                    }
                                }
                            })

                        }else {
                            let resultError = data?["result"]?.object
                            let error = resultError?["error"]?.object
                            let message = error?["message"]?.string
                            print("Failed = \(message)")
                            if let messageError = message {
                                showAlertCustomOneButton(self, title: "ThuyKieu", message: messageError, buttonFirst: "OK", completionHandle: {})
                            }

                        }
                    }else {
                        showAlertCustomOneButton(self, title: "ThuyKieu", message: "An error occur. please try again", buttonFirst: "OK", completionHandle: {})
                    }
                })
            }
        }else {
            showAlertCustomOneButton(self, title: "ThuyKieu", message: "Cần nhập đầy đủ thông tin!", buttonFirst: "OK", completionHandle: {})
        }
    }
}


