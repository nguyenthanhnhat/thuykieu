//
//  UserInfoObject.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 5/29/17.
//  Copyright © 2017 vn. All rights reserved.
//

import Foundation

class UserInfoObject: NSObject, NSCopying {

    var username : String
    var email : String
    var fullname : String
    var level : String
    var password : String
    var id : String

    override init() {
        self.username = ""
        self.email = ""
        self.fullname = ""
        self.level = ""
        self.password = ""
        self.id     = ""
    }

    init(username : String, email : String, fullname : String, level : String, password : String, id : String) {
        self.username = username
        self.email = email
        self.fullname =  fullname
        self.level =  level
        self.password = password
        self.id = id
    }

    func copy(with zone: NSZone?) -> Any {
        let copy = UserInfoObject(
            username : username,
            email : email,
            fullname :  fullname,
            level :  level,
            password : password,
            id : id
        )
        return copy
    }
}
