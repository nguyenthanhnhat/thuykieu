//
//  RegisterService.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 5/29/17.
//  Copyright © 2017 vn. All rights reserved.
//

import Foundation
import UIKit

class RegisterService {
    //    static let share : RegisterService = RegisterService()
    //MARK: CALL LOGIN SERVICE
    class func callLoginService(insecure : String , email:String, password: String, completionHandler: @escaping (Dictionary<String, AnyObject>?) -> ()) {
        let url : String = UrlMethod.LOGIN_SERVICE + "?username=" + email + "&password=" + password + "&insecure=" + insecure
        CoreServices.share.callThuykieuService(KeyHttpMethod.GET, urlString: url, bodyRequest: nil) { (dictionary) in
            completionHandler(dictionary)
        }
    }

    class func callRegisterService(userInfoObject : UserInfoObject , completionHandler: @escaping (Dictionary<String, AnyObject>?) -> ()) {
        let url : String = "http://thuykieu.com/api/user/register/?insecure=cool&username=" + userInfoObject.username + "&email=" + userInfoObject.email + "&display_name=" + userInfoObject.fullname  + "&user_pass=" + userInfoObject.password
        CoreServices.share.callThuykieuService(KeyHttpMethod.GET, urlString: url, bodyRequest: nil) { (dictionary) in
            completionHandler(dictionary)
        }
    }

    class func callCheckAccountExistingService(insecure : String , email:String, completionHandler: @escaping (Dictionary<String, AnyObject>?) -> ()) {
        let url = "http://thuykieu.com/api/user/retrieve_password/?insecure=cool&user_login=" +  email
        CoreServices.share.callThuykieuService(KeyHttpMethod.POST, urlString: url, bodyRequest: nil) { (dictionary) in
            completionHandler(dictionary)
        }
    }
}
