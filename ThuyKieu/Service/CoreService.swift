//
//  CoreService.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 5/29/17.
//  Copyright © 2017 vn. All rights reserved.
//

import Foundation
import Alamofire

let STATUS_CODE_RESPOND     = "statusCode"
let STATUS                  = "status"
let DATA_RESPOND            = "data"
let SUCCESS                 = "success"
let REQUEST_SUCCESS     = 200
let REQUEST_ERROR_404   = 404
let REQUEST_ERROR_403   = 403
let REQUEST_ERROR_401   = 401
let STATUS_SUCCESS = 0

class CoreServices  {
    static let share : CoreServices = CoreServices()
    fileprivate init(){}

    func callBackendService(_ methodHTTP: String, urlString: String, bodyRequest:NSDictionary?, completionHandler: @escaping
        (DataResponse<Any>?) -> ()) {
        log("urlString : \(urlString)")
        log("bodyRequest : \(String(describing: bodyRequest))")
        guard let urlRequest = createURLRequest(methodHTTP, urlString: urlString, bodyRequest: bodyRequest) else {
            completionHandler(nil)
            return
        }
        DispatchQueue.global(qos: .background).async {
            Alamofire.request(urlRequest).validate().responseJSON(completionHandler: { (dataResponse) in
                completionHandler(dataResponse)
            })
        }
    }


    func callThuykieuService(_ methodHTTP: String, urlString: String, bodyRequest:NSDictionary?, completionHandler: @escaping (Dictionary<String, AnyObject>?) -> ()) {

        self.callBackendService(methodHTTP, urlString: urlString, bodyRequest: bodyRequest) { (response) in
            var dataRespond : Dictionary<String, AnyObject> = [:]
            print("response \(response)")
            if (response == nil){
                completionHandler(nil)
                return
            }
            if let errorRespond = response?.error {
                if let err = NSError.handleGeneralErrorFromResponse(response?.response, error: errorRespond as NSError) {
                    dataRespond[STATUS_CODE_RESPOND]    = "\(err.code)" as AnyObject?
                    dataRespond[DATA_RESPOND]           = ["message" : "\(err.userInfo[KeyService.responseMessage]!)"] as AnyObject?
                    completionHandler(dataRespond as Dictionary?)
                    return
                }
            }
            if (response?.result.isSuccess)! {
                guard let dicResponse = response?.result.value else {
                    completionHandler(self.getOtherError() as Dictionary?)
                    return
                }
                dataRespond[STATUS_CODE_RESPOND]  = STATUS_SUCCESS as AnyObject?
                dataRespond[DATA_RESPOND]         = dicResponse as AnyObject?
                completionHandler(dataRespond as Dictionary?)
                return
            }else {
                guard let dicResponse = response?.data?.toDictionary() else {
                    completionHandler(self.getOtherError() as Dictionary?)
                    return
                }
                if  let code = dicResponse["code"] , let message = dicResponse["message"] {
                    dataRespond[STATUS_CODE_RESPOND]    = code as AnyObject?
                    dataRespond[DATA_RESPOND]           = ["message" : message] as AnyObject?
                    completionHandler(dataRespond as Dictionary?)
                    return
                }
            }
            completionHandler(self.getOtherError() as Dictionary?)
        }
    }
}

extension CoreServices {

    func initHeadersThuyKieu(mutableURLRequest: URLRequest) -> URLRequest{

        var mutableURLRequest = mutableURLRequest
        mutableURLRequest.addValue(KeyService.Accept_Type,          forHTTPHeaderField: KeyService.ContentType)
        return mutableURLRequest
    }

    func createURLRequest(_ methodHTTP: String, urlString: String, bodyRequest: NSDictionary?) -> URLRequest? {
        let urlRequest = URL(string: urlString)
        guard let url = urlRequest else {
            return nil
        }
        var mutableURLRequest = URLRequest(url: url)
        mutableURLRequest.httpMethod = methodHTTP
        mutableURLRequest.timeoutInterval = 30
        do {
            if let body = bodyRequest {
                mutableURLRequest.httpBody = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions())
            }
        } catch {}
        mutableURLRequest = initHeadersThuyKieu(mutableURLRequest: mutableURLRequest)
        log("HEADER \(String(describing: mutableURLRequest.allHTTPHeaderFields))")
        return mutableURLRequest
    }

    func getOtherError() -> Dictionary<String, AnyObject> {
        var dataRespond : Dictionary<String, AnyObject> = [:]
        dataRespond[STATUS_CODE_RESPOND]    = "111115" as AnyObject?
        dataRespond[DATA_RESPOND]           = ["message" : "An error occur. please try again"] as AnyObject?
        return dataRespond
    }
}
