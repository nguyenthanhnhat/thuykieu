//
//  ThuykieuParam.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 5/29/17.
//  Copyright © 2017 vn. All rights reserved.
//

import Foundation

enum ThuykieuParam {
    case login(String, String, String)
    case register(String, String, String, String, String, String)



    func toDictionary() -> NSDictionary {
        switch self {
        case .login(let insecure ,let email, let password): return ["insecure" : insecure, "email": email, "password": password]
        case .register(let insecure ,let username, let email, let display_name, let level_id, let user_pass): return ["insecure" : insecure, "username" : username, "email" : email, "display_name" : display_name, "level_id" : level_id, "user_pass" : user_pass]
        }
    }
}


