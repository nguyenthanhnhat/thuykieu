//
//  UIView.swift
//  Helio
//
//  Created by Dragon Member on 7/21/16.
//  Copyright © 2016 Helio LLC. All rights reserved.
//

import UIKit

public extension UIView {
    class func loadFromNibNamed(_ nibNamed: String, bundle : Bundle? = nil) -> UIView? {
        return UINib(nibName: nibNamed,bundle: bundle).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
   
}
