//
//  UIButton.swift
//  Helio
//
//  Created by Oanh Tran on 12/5/16.
//  Copyright © 2016 Helio LLC. All rights reserved.
//

import UIKit
import Foundation

extension UIButton {
    open override func awakeFromNib() {
        super.awakeFromNib()
        configureLabel()
    }
    
    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        configureLabel()
    }
    
    func configureLabel() {
        let sizeOfOldFont = self.titleLabel?.font.pointSize
        let nameOfOldFont = self.titleLabel?.font.fontName
        if nameOfOldFont!.contains("Oxygen") {
            let newNameFont = nameOfOldFont!.replacingOccurrences(of: "Oxygen", with: AppFonts.mainNameFont)
            self.titleLabel?.font = UIFont(name: newNameFont, size: sizeOfOldFont!)!
        }
    }
    
    
}
