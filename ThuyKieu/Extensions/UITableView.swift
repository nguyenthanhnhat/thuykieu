//
//  UITableView.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 5/14/17.
//  Copyright © 2017 vn. All rights reserved.
//

import Foundation
extension UITableView {
    func registerNib(nibName : String, identifier : String){
        self.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: identifier)
    }
}
