//
//  NSError+Extension.swift
//  Helio
//
//  Created by Rich Nguyen on 3/6/17.
//  Copyright © 2017 Helio. All rights reserved.
//

import Foundation

extension NSError {
  class func handleGeneralErrorFromResponse(_ response: HTTPURLResponse?, error: NSError) -> NSError? {
    var messageError : AnyObject?
    var userInfo : [AnyHashable: Any] = [:]
    var errClient :  NSError? = nil

    if error.isNoInternetConnectionError() {
      messageError    = "No internet" as AnyObject?
      userInfo        = [KeyService.responseMessage : messageError!]
        errClient       = NSError(domain: KeyService.domain, code: ErrorClient.NO_INTERNET_CONNECTION, userInfo: (userInfo as! [String : Any]))
    } else if error.isRequestTimeOutError() {
      messageError    = "Request time out" as AnyObject?
      userInfo        = [KeyService.responseMessage : messageError!]
        errClient       = NSError(domain: KeyService.domain, code: ErrorClient.REQUEST_TIME_OUT, userInfo: userInfo as! [String : Any])
    } else if let res = response {
      if res.isServerNotFound() {
        messageError    = "Server not found 404" as AnyObject?
        userInfo        = [KeyService.responseMessage : messageError!]
        errClient       = NSError(domain: KeyService.domain, code: ErrorClient.SERVER_IS_NOT_FOUND, userInfo: userInfo as! [String : Any])
      } else if res.isInternalError() {
        messageError    = "Internal Error 500" as AnyObject?
        userInfo        = [KeyService.responseMessage : messageError!]
        errClient       = NSError(domain: KeyService.domain, code: ErrorClient.INTERNAL_ERROR, userInfo: userInfo as! [String : Any])
      }/*  else if res.isErrorCode400() {
        messageError    = "error code 400" as AnyObject?
        userInfo        = [KeyService.responseMessage : messageError!]
        errClient       = NSError(domain: KeyService.domain, code: ErrorClient.SYSTEM_ERROR, userInfo: userInfo)
      }*/
//     else {
//      messageError = "An error occur. please try again" as AnyObject?
//      userInfo = [KeyService.responseMessage : messageError!]
//      errClient = NSError(domain: KeyService.domain, code: ErrorClient.SYSTEM_ERROR, userInfo: userInfo)
//    }
    }
    return errClient
  }
}
