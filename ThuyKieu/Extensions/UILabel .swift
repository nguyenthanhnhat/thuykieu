//
//  UILabel .swift
//  Helio
//
//  Created by Oanh Tran on 10/24/16.
//  Copyright © 2016 Helio LLC. All rights reserved.
//

import Foundation

import UIKit

extension UILabel {
    public func startBlinking() -> Int {
        self.alpha = 1
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.repeat, .autoreverse], animations:
            {
                self.alpha = 0
            }, completion: nil)
        return self.tag
    }
    
    /**
     Tells the label to stop blinking.
     */
    public func stopBlinking() -> Int {
        self.alpha = 1
        self.layer.removeAllAnimations()
        return 0
    }
    //MARK: CHANGE FONT
    open override func awakeFromNib() {
        super.awakeFromNib()
        configureLabel()
    }
    
    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        configureLabel()
    }
    
    func configureLabel() {
        let sizeOfOldFont = self.font.pointSize
        let nameOfOldFont = self.font.fontName
        if nameOfOldFont.contains("Oxygen") {
            let newNameFont = nameOfOldFont.replacingOccurrences(of: "Oxygen", with: AppFonts.mainNameFont)
            self.font = UIFont(name: newNameFont, size: sizeOfOldFont)
        }
    }
    func setSubTextColor(_ pSubString : String, pColor : UIColor, pFont: UIFont){
        let attributedString: NSMutableAttributedString = self.attributedText != nil ? NSMutableAttributedString(attributedString: self.attributedText!) : NSMutableAttributedString(string: self.text!);
        let range = attributedString.mutableString.range(of: pSubString)
        if range.location != NSNotFound {
            attributedString.addAttributes([NSAttributedStringKey.foregroundColor : pColor,
                                            NSAttributedStringKey.font : pFont], range: range)
        }
        self.attributedText = attributedString
    }
}
