//
//  Unit.swift
//  Helio
//
//  Created by Oanh Tran on 10/6/16.
//  Copyright © 2016 Helio LLC. All rights reserved.
//

import UIKit


extension Double {
    func roundTo(place: Int)-> Double{
        let divisor = pow(10.0, Double(place))
        return (self * divisor).rounded() / divisor
    }
}
