//
//  AppDelegateExtension.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 5/29/17.
//  Copyright © 2017 vn. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import IQKeyboardManagerSwift
//MARK : DELEGATE APPDELEGATE
extension AppDelegate {


    func getHeightScreen() {
        self.heightScreen = Int((self.window?.bounds.height)!)
    }


    func showActivityWithLabel(string: String) {

        var rootView = self.window!.rootViewController!.view
        if let presentedViewController = self.window!.rootViewController!.presentedViewController {
            rootView = presentedViewController.view
        }
        self.hideActivity()
        self.activityIndicator = MBProgressHUD.init(view: rootView!)
        self.activityIndicator!.removeFromSuperViewOnHide = true
        self.activityIndicator!.label.text = string
        self.activityIndicator!.label.textColor = UIColor.white
        self.activityIndicator?.contentColor = UIColor.black
        self.activityIndicator!.bezelView.color = AppColors.backgroundRegisterColor
        rootView!.addSubview(self.activityIndicator!)
        self.activityIndicator!.show(animated: true)
    }

    func hideActivity() {
        if self.activityIndicator != nil {
            self.activityIndicator?.hide(animated: true)
            self.activityIndicator = nil
        }
    }

    func checkConectionInternet() {
        if !connectInternet() {
            if let vc = GET_APP_DELEGATE.window?.rootViewController {
                if(connectInternetAlertController == nil){
                    connectInternetAlertController = UIAlertController(title: "No Connect Internet", message: "Please connect wifi/3G to use Helio Application", preferredStyle: .alert)
                    vc.present(connectInternetAlertController!, animated: true, completion: nil)
                }
            }
        }else{
            if (connectInternetAlertController != nil){
                connectInternetAlertController?.dismiss(animated: true, completion: nil)
                connectInternetAlertController = nil
            }
        }
    }
}

