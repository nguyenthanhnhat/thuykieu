//
//  UITextField.swift
//  Helio
//
//  Created by Dragon Member on 10/24/16.
//  Copyright © 2016 Helio LLC. All rights reserved.
//
import UIKit
import Foundation

extension UITextField {
    func blink() {
        self.alpha = 0.0;
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .repeat, animations: {
            self.alpha = 0.0
            }, completion: nil)
        UIView.animate(withDuration: 0.9, delay: 0.0, options: .repeat, animations: {
            self.alpha = 1.0
            }, completion: nil)
    }
    
    public func stopBlink() {
        self.alpha = 1.0
        self.layer.removeAllAnimations()
    }
    
    func bigSizeCurrentTextField() {
        self.font = UIFont(name: AppFonts.oxygenRegular , size: 40)
//        self.textColor = AppColors.pinkColor

    }
    //MARK: CHANGE FONT
    open override func awakeFromNib() {
        super.awakeFromNib()
        configureLabel()
    }
    
    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        configureLabel()
    }
    
    func configureLabel() {
        let sizeOfOldFont = self.font!.pointSize
        let nameOfOldFont = self.font!.fontName
        if nameOfOldFont.contains("Oxygen") {
            let newNameFont = nameOfOldFont.replacingOccurrences(of: "Oxygen", with: AppFonts.mainNameFont)
            self.font = UIFont(name: newNameFont, size: sizeOfOldFont)
        }
    }

    
}
