//
//  UIViewController.swift
//  Helio
//
//  Created by Dragon Member on 7/26/16.
//  Copyright © 2016 Helio LLC. All rights reserved.
//

import UIKit

extension UIViewController {
    func popToViewController(_ viewName: AnyObject.Type){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for i in 0...viewControllers.count-1 {
                if(self.navigationController?.viewControllers[i].isKind(of: viewName) == true) {
                    _ = self.navigationController?.popToViewController(self.navigationController!.viewControllers[i], animated: true)
                    break
                }
            }
    }
    
    func handleErrorAfterCallService(_ error : NSError, enterCase : String) {
        if(error.domain == KeyService.domain) {
            handleClientErrorWithCode(error.code, enterCase: "")
        } else {
            
        }
    }

    func changeMainViewController(_ viewController : UIViewController){
        let nv = UINavigationController(rootViewController: viewController)
        nv.interactivePopGestureRecognizer!.isEnabled = false
        nv.isNavigationBarHidden = true
        GET_APP_DELEGATE.slideViewController?.changeMainViewController(nv, close: true)
    }
    
    func pushViewController(ViewController : UIViewController){
        self.navigationController?.pushViewController(ViewController, animated: true)
    }
    func pushViewControllerInTab(ViewController : UIViewController){
        let navi = GET_APP_DELEGATE.slideViewController?.mainViewController as! UINavigationController
        navi.pushViewController(ViewController, animated: true)
    }
    
    func presentViewController(ViewController: UIViewController) {
        GET_APP_DELEGATE.slideViewController?.mainViewController?.present(ViewController, animated: true, completion: nil)
    }
    
    fileprivate func handleClientErrorWithCode(_ errorCode:Int, enterCase: String?){
        
        switch (errorCode){
        case ErrorClient.REQUEST_TIME_OUT:
            Popups.SharedInstance.ShowAlert(self, title: "TITLE".localized(), message: "Request timeout" , buttons: ["OK".localized()]) {(buttonPressed) -> Void in
                if buttonPressed == "OK".localized() {
                    //to do
                }
            }
            break
        case ErrorClient.NO_INTERNET_CONNECTION:
            Popups.SharedInstance.ShowAlert(self, title: "TITLE".localized(), message: "No Internet Connection" , buttons: ["OK".localized()]) {(buttonPressed) -> Void in
                if buttonPressed == "OK".localized() {
                    //to do
                }
            }
            break
        case ErrorClient.SERVER_IS_NOT_FOUND:
            Popups.SharedInstance.ShowAlert(self, title: "TITLE".localized(), message: "Server is not found" , buttons: ["OK".localized()]) {(buttonPressed) -> Void in
                if buttonPressed == "OK".localized() {
                    //to do
                }
            }
            break
        case ErrorClient.INTERNAL_ERROR:
            Popups.SharedInstance.ShowAlert(self, title: "TITLE".localized(), message: "Internal Error" , buttons: ["OK".localized()]) {(buttonPressed) -> Void in
                if buttonPressed == "OK".localized() {
                    //to do
                }
            }
            break
        default:
            break
        }
    }
    
    
}
