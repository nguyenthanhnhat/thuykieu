//
//  DateFormatter.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 4/24/17.
//  Copyright © 2017 ThuyKieu Company. All rights reserved.
//

import Foundation

extension DateFormatter {
    static func createDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter
    }
}
