//
//  String.swift
//  Helio
//
//  Created by Dragon Member on 7/21/16.
//  Copyright © 2016 Helio LLC. All rights reserved.
//

import Foundation

public extension String {
    var length: Int {
        return characters.count
    }
    func localized() -> String {
        if let dico = GET_APP_DELEGATE.localisationFile {
            if let localizedString = dico[self] as? String {
                // If the localization exists, return it
                return localizedString
            }  else {
                // Returns the key if no String was found
                return self
            }
        } else {
            // If not, we can use the NSLocalizedString function
            return NSLocalizedString(self, comment: "") 
        }
    }
    
    struct NumberFormatter {
        static let instance = Foundation.NumberFormatter()
    }
    
    var floatValue: Float? {
        return (self as NSString).floatValue
    }
    
    var doubleValue: Double? {
        return NumberFormatter.instance.number(from: self)?.doubleValue
    }
    
    var integerValue: Int? {
        return NumberFormatter.instance.number(from: self)?.intValue
    }
    
    func base64Encoded() -> String {
        let plainData = data(using: String.Encoding.utf8)
        let base64String = plainData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        return base64String!
    }
    
    func base64Decoded() -> String {
        let decodedData = Data(base64Encoded: self, options: NSData.Base64DecodingOptions(rawValue: 0))
        let decodedString = String(data: decodedData!, encoding: String.Encoding.utf8)
        return decodedString!
    }
    
    func insert(_ string: String, ind: Int) -> String {
        return String(self.characters.prefix(ind)) + string + String(self.characters.suffix(self.characters.count - ind))
    }
    
    func replace(_ string: String, replacement: String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(" ", replacement: "")
    }
    
//    func split(_ len: Int) -> [String] {
//        var currentIndex = 0
//        var array = [String]()
//        let length = self.characters.count
//        while currentIndex < length {
//            let startIndex = self.characters.index(self.startIndex, offsetBy: currentIndex)
//            let endIndex = <#T##String.CharacterView corresponding to `startIndex`##String.CharacterView#>.index(startIndex, offsetBy: len, limitedBy: self.endIndex)
//            let substr = self.substring(with: (startIndex ..< endIndex!))
//            array.append(substr)
//            currentIndex += len
//        }
//        return array    }
    
//    func split(_ str: String, _ count: Int) -> [String] {
//        return stride(from: 0, to: str.characters.count, by: count).map { i -> String in
//            let startIndex = str.index(str.startIndex, offsetBy: i)
//            let endIndex   = str.index(startIndex, offsetBy: count, limitedBy: str.endIndex) ?? str.endIndex
//            return str[startIndex..<endIndex]
//        }
//    }
    
    func substringWithRange(_ start: Int, end: Int) -> String
    {
        if (start < 0 || start > self.characters.count)
        {
            print("start index \(start) out of bounds")
            return ""
        }
        else if end < 0 || end > self.characters.count
        {
            print("end index \(end) out of bounds")
            return ""
        }
        let range = (self.characters.index(self.startIndex, offsetBy: start) ..< self.characters.index(self.startIndex, offsetBy: end))
        return self.substring(with: range)
    }
    
    func substringWithRange(_ start: Int, location: Int) -> String
    {
        if (start < 0 || start > self.characters.count)
        {
            print("start index \(start) out of bounds")
            return ""
        }
        else if location < 0 || start + location > self.characters.count
        {
            print("end index \(start + location) out of bounds")
            return ""
        }
        let range = (self.characters.index(self.startIndex, offsetBy: start) ..< self.characters.index(self.startIndex, offsetBy: start + location))
        return self.substring(with: range)
    }

    func trim() -> String{
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
    }
    func containsEmoji() -> Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x3030, 0x00AE, 0x00A9,// Special Characters
            0x1D000...0x1F77F,          // Emoticons
            0x2100...0x27BF,            // Misc symbols and Dingbats
            0xFE00...0xFE0F,            // Variation Selectors
            0x1F900...0x1F9FF:          // Supplemental Symbols and Pictographs
                return true
            default:
                continue
            }
        }
        return false
    }
   
    
}
