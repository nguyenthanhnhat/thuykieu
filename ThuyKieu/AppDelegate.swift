//
//  AppDelegate.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 5/9/17.
//  Copyright © 2017 vn. All rights reserved.
//

import UIKit
import FBSDKShareKit
import FBSDKLoginKit
import Stripe
import IQKeyboardManagerSwift
import MBProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var localisationFile    : NSDictionary?
    var slideViewController     : SlideMenuController?
    var mainMenuViewController  : MainMenuViewController?
    var homeViewController      : HomeViewController?
    var welcomeViewController   : WelcomeViewController?
    var sigInViewController     : SignInViewController?
    var heightScreen            : Int = 0
    var activityIndicator       : MBProgressHUD?
    var connectInternetAlertController  : UIAlertController? = nil

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:
        [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        setWelcomeToRootViewController()
        IQKeyboardManager.shared.enable = true
        STPPaymentConfiguration.shared().publishableKey = "pk_test_6pRNASCoBOKtIshFeQd4XMUh"
        CategoriesService.callGetCategories()
//        Thread.sleep(forTimeInterval: 4.0)

        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }


    func setWelcomeToRootViewController(){
        welcomeViewController = WelcomeViewController(nibName: WELCOME_VIEW_CONTROLER, bundle: nil)
        let navi = UINavigationController(rootViewController: welcomeViewController!)
        window?.rootViewController = navi
    }

    func setSigInToRootViewController(){
        let sigInViewController1 = initViewController(REGISTER_STORYBOARD, storyid: "SignInViewController")
        let navi = UINavigationController(rootViewController: sigInViewController1)
        window?.rootViewController = navi
    }



    func initSlideViewController(){
        mainMenuViewController  = MainMenuViewController(nibName: MAIN_MENU_VIEW_CONTROLLER, bundle: nil)
        homeViewController      = HomeViewController(nibName: HOME_VIEW_CONTROLLER, bundle: nil)
        let navi = UINavigationController(rootViewController: homeViewController!)
        navi.isNavigationBarHidden = true
        slideViewController     = SlideMenuController(mainViewController: navi, leftMenuViewController: mainMenuViewController!)
        slideViewController?.changeLeftViewWidth(250)

    }

    func setSlideToRootViewController(){
        initSlideViewController()
        self.window?.rootViewController = slideViewController
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {

    }

    func applicationWillEnterForeground(_ application: UIApplication) {

    }

    func applicationDidBecomeActive(_ application: UIApplication) {

    }

    func applicationWillTerminate(_ application: UIApplication) {

    }

    func setDefaultLanguage(){

    }



}

