
import UIKit
@objc protocol RegisterNavigationViewDelegate : class {
    @objc optional func didClickBack()
    
}
class RegisterNavigationView: UIView {

    @IBOutlet weak var tittleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var openTimeLabel: UILabel!
    
    weak var delegate : RegisterNavigationViewDelegate?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBAction func clickBackButton(_ sender: Any) {
        delegate?.didClickBack!()
    }

}
