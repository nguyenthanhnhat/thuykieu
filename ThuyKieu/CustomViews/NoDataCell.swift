//
//  NoDataCell.swift
//  ThuyKieu
//
//  Created by Thanh Nhat on 6/10/17.
//  Copyright © 2017 vn. All rights reserved.
//

import UIKit

class NoDataCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
