//
//  FilterNavigationView.swift
//  Helio
//
//  Created by Rich Nguyen on 3/4/17.
//  Copyright © 2017 Helio Company. All rights reserved.
//

import UIKit
@objc protocol FilterNavigationViewDelegate {
    @objc optional func didClickApply()
    
}
class FilterNavigationView: UIView {

    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var filterLabel: UILabel!
    
    var delegate : FilterNavigationViewDelegate?
    @IBAction func clickClear(_ sender: Any) {
//        let vc = viewController(self)
//        vc.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickApply(_ sender: Any) {
//        delegate?.didClickApply!()
//        let vc = viewController(self)
//        vc.dismiss(animated: true, completion: nil)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
