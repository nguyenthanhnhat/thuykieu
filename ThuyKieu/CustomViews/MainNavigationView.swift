

import UIKit
@objc protocol MainNavigationViewDelegate : class {
    @objc optional func didClickMainMenu()
    @objc optional func didClickUserMenu()
    
}
class MainNavigationView: UIView {

    @IBOutlet weak var tittleLabel: UILabel!
    @IBOutlet weak var mainMenuButton: UIButton!
    @IBOutlet weak var userMenuButton: UIButton!
    
    weak var delegate : MainNavigationViewDelegate?

    @IBAction func clickMainMenu(_ sender: Any) {
        delegate?.didClickMainMenu!()
    }
    
    @IBAction func clickUserMenu(_ sender: Any) {
        delegate?.didClickUserMenu!()
    }
}
