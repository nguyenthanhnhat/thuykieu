

import UIKit

@objc protocol CustomizeNavigationViewDelegate {
    @objc optional func didClickBack()
    @objc optional func didClickShare()
}

class CustomizeNavigationView: UIView {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var tittleLabel: UILabel!


    var delegate : CustomizeNavigationViewDelegate?

    override func awakeFromNib() {
    }

    @IBAction func clickBack(_ sender: Any) {
        let vc = getViewController(self)
        _ = vc.navigationController?.popViewController(animated: true)
        delegate?.didClickBack!()
    }
    
    @IBAction func clickShare(_ sender: Any) {
        log("SHARE CLICK")
        delegate?.didClickShare!()
    }

}
