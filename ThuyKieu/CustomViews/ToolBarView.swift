
import UIKit
@objc protocol ToolBarViewDelegate : class {
    @objc optional func didClickFilter()
    @objc optional func didClickMap()
}

class ToolBarView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    weak var delegate : ToolBarViewDelegate?
    var isMap : Bool = true
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var mapButton: UIButton!
    @IBAction func clickFilter(_ sender: Any) {
        delegate?.didClickFilter!()
    }
    
    @IBAction func clickMap(_ sender: Any) {
        isMap = !isMap
        let text = isMap ? "Map" : "Info"
        mapButton.setTitle(text, for: UIControlState.normal)
        delegate?.didClickMap!()
    }
    
}
